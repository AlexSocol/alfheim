package alfheim.api.lib

import cpw.mods.fml.client.registry.RenderingRegistry

object LibRenderIDs {
	
	val idAnyavil = RenderingRegistry.getNextAvailableRenderId()
	val idBarrel = RenderingRegistry.getNextAvailableRenderId()
	val idChair = RenderingRegistry.getNextAvailableRenderId()
	val idComposite = RenderingRegistry.getNextAvailableRenderId()
	val idDomainDoor = RenderingRegistry.getNextAvailableRenderId()
	val idDoubleBlock = RenderingRegistry.getNextAvailableRenderId()
	val idDoubleFlower = RenderingRegistry.getNextAvailableRenderId()
	val idFloodlight = RenderingRegistry.getNextAvailableRenderId()
	val idGrapeRedPlanted = RenderingRegistry.getNextAvailableRenderId()
	val idGrapeWhite = RenderingRegistry.getNextAvailableRenderId()
	val idHarvester = RenderingRegistry.getNextAvailableRenderId()
	val idHopper = RenderingRegistry.getNextAvailableRenderId()
	val idManaAccelerator = RenderingRegistry.getNextAvailableRenderId()
	val idManaReflector = RenderingRegistry.getNextAvailableRenderId()
	val idManaTuner = RenderingRegistry.getNextAvailableRenderId()
	val idMultipass = RenderingRegistry.getNextAvailableRenderId()
	val idNiflheim = RenderingRegistry.getNextAvailableRenderId()
	val idNidhoggTooth = RenderingRegistry.getNextAvailableRenderId()
	val idPowerStone = RenderingRegistry.getNextAvailableRenderId()
	val idPylon = RenderingRegistry.getNextAvailableRenderId()
	val idSimpleDoubleBlock = RenderingRegistry.getNextAvailableRenderId()
	val idShrinePanel = RenderingRegistry.getNextAvailableRenderId()
	val idSpire = RenderingRegistry.getNextAvailableRenderId()
	val idTable = RenderingRegistry.getNextAvailableRenderId()
	val idWorldTree = RenderingRegistry.getNextAvailableRenderId()
}
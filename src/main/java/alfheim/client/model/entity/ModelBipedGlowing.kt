package alfheim.client.model.entity

import alexsocol.asjlib.glScaled
import alexsocol.asjlib.render.ASJRenderHelper
import net.minecraft.client.model.ModelBiped
import net.minecraft.entity.Entity
import org.lwjgl.opengl.GL11.*

class ModelBipedGlowing: ModelBiped() {
	
	override fun render(entity: Entity?, f: Float, f1: Float, f2: Float, f3: Float, f4: Float, f5: Float) {
		ASJRenderHelper.setGlow()
		
		if (isChild) {
			glPushMatrix()
			glScaled(0.75)
			glTranslatef(0f, 16f * f5, 0f)
		}
		
		super.render(entity, f, f1, f2, f3, f4, f5)
		
		if (isChild) glPopMatrix()
		
		ASJRenderHelper.discard()
	}
}
package alfheim.common.item.relic

import alexsocol.asjlib.*
import alexsocol.asjlib.math.Vector3
import alfheim.api.AlfheimAPI
import alfheim.api.item.ISpeedUpItem
import alfheim.common.achievement.AlfheimAchievements
import alfheim.common.core.helper.*
import alfheim.common.core.util.AlfheimTab
import net.minecraft.enchantment.EnchantmentHelper
import net.minecraft.entity.*
import net.minecraft.entity.monster.IMob
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.entity.projectile.EntityThrowable
import net.minecraft.item.ItemStack
import net.minecraft.potion.Potion
import net.minecraft.server.MinecraftServer
import net.minecraft.stats.Achievement
import net.minecraft.util.*
import net.minecraft.world.World
import vazkii.botania.api.BotaniaAPI
import vazkii.botania.api.internal.IManaBurst
import vazkii.botania.api.item.IRelic
import vazkii.botania.api.mana.*
import vazkii.botania.common.core.helper.ItemNBTHelper
import vazkii.botania.common.entity.EntityManaBurst
import vazkii.botania.common.item.equipment.tool.manasteel.ItemManasteelSword
import vazkii.botania.common.item.relic.ItemRelic
import java.util.*

/**
 * This code is completely copied from 208th Botania version and fully made by Vazkii or whoever... :D<br></br>
 * Hope all required stuff is already done by Botania using iterfaces and stuff...
 */
class ItemExcaliber: ItemManasteelSword(AlfheimAPI.EXCALIBER, "Excaliber"), IRelic, ILensEffect, ISpeedUpItem {
	
	init {
		creativeTab = AlfheimTab
	}
	
	override fun onUpdate(stack: ItemStack, world: World, player: Entity?, slotID: Int, inHand: Boolean) {
		if (player !is EntityPlayer) return
		ItemRelic.updateRelic(stack, player)
		
		val haste = player.getActivePotionEffect(Potion.digSpeed.id)
		val check = if (haste == null) 1f / 6f else if (haste.getAmplifier() == 0) 0.4f else if (haste.getAmplifier() == 2) 1f / 3f else 0.5f
		if (world.isRemote || !inHand || player.swingProgress != check) return
		getBurst(player, stack).spawn()
		player.playSoundAtEntity("botania:terraBlade", 0.4f, 1.4f)
	}
	
	override fun getIsRepairable(stack: ItemStack?, material: ItemStack?) = false
	
	override fun addInformation(stack: ItemStack?, player: EntityPlayer?, infoList: List<Any?>, advTooltip: Boolean) = ItemRelic.addBindInfo(infoList, stack, player)
	
	override fun bindToUsername(playerName: String, stack: ItemStack) = ItemRelic.bindToUsernameS(playerName, stack)
	
	override fun getSoulbindUsername(stack: ItemStack) = ItemRelic.getSoulbindUsernameS(stack)!!
	
	override fun getBindAchievement() = AlfheimAchievements.excaliber
	
	override fun setBindAchievement(achievement: Achievement) = Unit
	
	override fun usesMana(stack: ItemStack?) = false
	
	override fun isItemTool(p_77616_1_: ItemStack) = true
	
	override fun getEntityLifespan(itemStack: ItemStack?, world: World?) = Integer.MAX_VALUE
	
	fun getBurst(player: EntityPlayer, stack: ItemStack): EntityManaBurst {
		val burst = EntityManaBurst(player)
		
		val motionModifier = 7f
		
		burst.color = 0xFFFF20
		burst.mana = 1
		burst.startingMana = 1
		burst.minManaLoss = 200
		burst.manaLossPerTick = 1f
		burst.gravity = 0f
		burst.setMotion(burst.motionX * motionModifier, burst.motionY * motionModifier, burst.motionZ * motionModifier)
		
		val lens = stack.copy()
		ItemNBTHelper.setString(lens, TAG_ATTACKER_USERNAME, player.commandSenderName)
		val damage = player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.attackDamage).attributeValue
		ItemNBTHelper.setDouble(lens, TAG_DAMAGE, damage)
		burst.sourceLens = lens
		return burst
	}
	
	override fun apply(stack: ItemStack, props: BurstProperties) = Unit
	
	override fun collideBurst(burst: IManaBurst, pos: MovingObjectPosition, isManaBlock: Boolean, dead: Boolean, stack: ItemStack) = dead
	
	override fun updateBurst(burst: IManaBurst, stack: ItemStack) {
		burst as EntityThrowable
		
		val axis = getBoundingBox(burst.posX, burst.posY, burst.posZ, burst.lastTickPosX, burst.lastTickPosY, burst.lastTickPosZ).expand(1)
		
		val attacker = ItemNBTHelper.getString(burst.sourceLens, TAG_ATTACKER_USERNAME, "")
		var homeID = ItemNBTHelper.getInt(stack, TAG_HOME_ID, -1)
		if (homeID == -1) {
			val axis1 = getBoundingBox(burst.posX, burst.posY, burst.posZ, burst.lastTickPosX, burst.lastTickPosY, burst.lastTickPosZ).expand(5)
			val entities = getEntitiesWithinAABB(burst.worldObj, EntityLivingBase::class.java, axis1)
			entities.forEach {
				if (it is EntityPlayer || it !is IMob || it.hurtTime != 0) return@forEach
				homeID = it.entityId
				ItemNBTHelper.setInt(stack, TAG_HOME_ID, homeID)
			}
		}
		val entities = getEntitiesWithinAABB(burst.worldObj, EntityLivingBase::class.java, axis)
		val home: Entity?
		if (homeID != -1) {
			home = burst.worldObj.getEntityByID(homeID)
			if (home != null && home.isEntityAlive) {
				val vecMotion = Vector3.fromEntityCenter(home).sub(Vector3.fromEntityCenter(burst))
				vecMotion.normalize().mul(Vector3(burst.motionX, burst.motionY, burst.motionZ).length())
				burst.setMotion(vecMotion.x, vecMotion.y, vecMotion.z)
			} else {
				ItemNBTHelper.setInt(stack, TAG_HOME_ID, -1)
			}
		}
		
		if (burst.isFake || burst.worldObj.isRemote) return
		
		entities.forEach {
			if (it is EntityPlayer && !(it.commandSenderName != attacker && (MinecraftServer.getServer() == null || MinecraftServer.getServer().isPVPEnabled))) return@forEach
			if (it.hurtTime != 0) return@forEach
			
			val player = it.worldObj.getPlayerEntityByName(attacker)
			var damage = ItemNBTHelper.getDouble(stack, TAG_DAMAGE, 4.0 + AlfheimAPI.EXCALIBER.damageVsEntity)
			if (player != null) damage += EnchantmentHelper.func_152377_a(stack, it.creatureAttribute)
			it.attackEntityFrom(if (player == null) DamageSource.magic else DamageSource.causePlayerDamage(player).setDamageBypassesArmor().setMagicDamage().setTo(ElementalDamage.LIGHTNESS), damage.F)
			burst.setDead()
			return
		}
	}
	
	override fun doParticles(burst: IManaBurst, stack: ItemStack) = true
	
	override fun getRarity(sta: ItemStack) = BotaniaAPI.rarityRelic!!
	
	override fun getSpeedUp(wearer: EntityLivingBase, stack: ItemStack) = 0.05f
	
	companion object {
		
		val uuid = UUID.fromString("7d5ddaf0-15d2-435c-8310-bdfc5fd1522d")!!
		
		private const val TAG_ATTACKER_USERNAME = "attackerUsername"
		private const val TAG_DAMAGE = "damage"
		private const val TAG_HOME_ID = "homeID"
	}
}

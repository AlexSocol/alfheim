package alfheim.common.world.dim.alfheim.structure

import alexsocol.asjlib.*
import alexsocol.asjlib.math.Vector3
import alfheim.api.ModInfo
import alfheim.common.block.tile.TilePowerStone
import alfheim.common.world.data.CustomWorldData.Companion.customData
import alfheim.common.world.dim.alfheim.biome.BiomeField
import net.minecraft.world.World
import ru.vamig.worldengine.*
import ru.vamig.worldengine.standardcustomgen.StructureBaseClass
import java.util.*

object StructureShrine: StructureBaseClass() {
	
	val shrines = listOf(
		"${ModInfo.MODID}/schemas/shrineBerserk",
		"${ModInfo.MODID}/schemas/shrineNinja",
		"${ModInfo.MODID}/schemas/shrineOvermage",
		"${ModInfo.MODID}/schemas/shrineTank"
	)
	
	override fun generate(world: World, rand: Random, x: Int, y: Int, z: Int, chunkProvider: WE_ChunkProvider): Boolean {
		if (ASJUtilities.isClient) return false // just in case
		if (x shr 4 in -32 until 32 || z shr 4 in -32 until 32) return false // no shrines in Yggdrasil pit
		
		arrayOf(-1 to -1, 1 to 1, -1 to 1, 1 to -1).forEach { (i, k) ->
			if (WE_Biome.getBiomeAt(chunkProvider, x + i * 16, z + k * 16) !== BiomeField)
				return false
		}
		
		val data = world.customData
		val locs = data.structures["any"]
		
		if (locs.any { Vector3.pointDistancePlane(x, z, it.first, it.second) < 128 }) return false
		
		SchemaUtils.generate(world, x, y, z, shrines.random(rand)!!, true)

		for (i in 0..2) {
			val tile = world.getTileEntity(x, y + i, z) as? TilePowerStone ?: continue
			tile.lock(x, y + i, z, world.provider.dimensionId)
		}
		
		locs.add(x to z)
		data.markDirty()
		
		return true
	}
}

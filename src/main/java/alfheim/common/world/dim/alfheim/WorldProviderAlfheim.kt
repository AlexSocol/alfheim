package alfheim.common.world.dim.alfheim

import alexsocol.asjlib.ASJUtilities
import alfheim.client.render.world.*
import alfheim.common.block.AlfheimBlocks
import alfheim.common.core.handler.AlfheimConfigHandler
import alfheim.common.core.handler.AlfheimConfigHandler.enableAlfheimRespawn
import alfheim.common.core.handler.AlfheimConfigHandler.enableElvenStory
import alfheim.common.core.handler.ragnarok.RagnarokHandler
import alfheim.common.world.data.CustomWorldData.Companion.customData
import alfheim.common.world.dim.alfheim.biome.*
import alfheim.common.world.dim.alfheim.customgens.*
import net.minecraft.util.ChunkCoordinates
import net.minecraft.world.biome.BiomeGenBase
import ru.vamig.worldengine.*
import ru.vamig.worldengine.standardcustomgen.*
import vazkii.botania.common.Botania
import vazkii.botania.common.block.ModBlocks

class WorldProviderAlfheim: WE_WorldProvider() {
	
	val worldFeatures = true
	
	override fun genSettings(cp: WE_ChunkProvider) {
		cp.createChunkGen_List.clear()
		cp.decorateChunkGen_List.clear()
		
		WE_Biome.setBiomeMap(cp, 1.2, 6, 8000.0, 0.4)
		
		val terrainGenerator = WE_TerrainGenerator()
		terrainGenerator.worldStoneBlock = ModBlocks.livingrock
		terrainGenerator.worldSeaGenMaxY += BiomeAlfheim.offset
		cp.createChunkGen_List.add(terrainGenerator)
		
		cp.createChunkGen_List.add(YggdrasilGenerator)
		cp.createChunkGen_List.add(NiflheimLocationGenerator)
		
		if (worldFeatures) {
			val cg = WE_CaveGen()
			cg.replaceBlocksList.clear()
			cg.replaceBlocksMetaList.clear()
			cg.addReplacingBlock(ModBlocks.livingrock, 0.toByte())
			cp.createChunkGen_List.add(cg)
			val rg = WE_RavineGen()
			rg.replaceBlocksList.clear()
			rg.replaceBlocksMetaList.clear()
			rg.addReplacingBlock(ModBlocks.livingrock, 0.toByte())
			cp.createChunkGen_List.add(rg)
		}
		
		val ores = WE_OreGen()
		val m = AlfheimConfigHandler.oregenMultiplier
		ores.add(AlfheimBlocks.elvenOre, ModBlocks.livingrock, 0, 1, 8, 1 * m, 2 * m, 75, 1, 16)  // Dragonstone
		ores.add(AlfheimBlocks.elvenOre, ModBlocks.livingrock, 1, 1, 8, 3 * m, 6 * m, 100, 1, 64) // Elementium
		ores.add(AlfheimBlocks.elvenOre, ModBlocks.livingrock, 2, 4, 8, 1 * m, 1 * m, 100, 1, 48) // Quartz
		ores.add(AlfheimBlocks.elvenOre, ModBlocks.livingrock, 3, 1, 8, 2 * m, 3 * m, 100, 1, 32) // Gold
		ores.add(AlfheimBlocks.elvenOre, ModBlocks.livingrock, 4, 1, 4, 1 * m, 1 * m, 50, 1, 16)  // Iffesal
		ores.add(AlfheimBlocks.elvenOre, ModBlocks.livingrock, 5, 4, 8, 1 * m, 1 * m, 100, 1, 48) // Lapis
		
		cp.decorateChunkGen_List.add(WorldGenAlfheim)
		if (worldFeatures) {
			cp.decorateChunkGen_List.add(ores)
			if (Botania.thaumcraftLoaded)
				cp.decorateChunkGen_List.add(WorldGenAlfheimThaumcraft)
			
			cp.decorateChunkGen_List.add(AlfheimLakeGen())
		}
		
		WE_Biome.addBiomeToGeneration(cp, BiomeField)
		WE_Biome.addBiomeToGeneration(cp, BiomeIslandGiantFlowers)
		WE_Biome.addBiomeToGeneration(cp, BiomeBeach)
		WE_Biome.addBiomeToGeneration(cp, BiomeSandbank)
		WE_Biome.addBiomeToGeneration(cp, BiomeRiver)
		WE_Biome.addBiomeToGeneration(cp, BiomeMountLow)
		WE_Biome.addBiomeToGeneration(cp, BiomeMountMid)
		WE_Biome.addBiomeToGeneration(cp, BiomeMountHigh)
		WE_Biome.addBiomeToGeneration(cp, BiomeMountTopForest)
		WE_Biome.addBiomeToGeneration(cp, BiomeMountTopField)
		WE_Biome.addBiomeToGeneration(cp, BiomeIslandForest)
		WE_Biome.addBiomeToGeneration(cp, BiomePitForest)
		
		if (!worldFeatures)
			listOf(BiomeField,
				   BiomeIslandGiantFlowers,
				   BiomeBeach,
				   BiomeSandbank,
				   BiomeRiver,
				   BiomeMountLow,
				   BiomeMountMid,
				   BiomeMountHigh,
				   BiomeMountTopForest,
				   BiomeMountTopField,
				   BiomeIslandForest,
				   BiomePitForest).forEach {
					   it.decorateChunkGen_List.clear()
		}
	}
	
	override fun getDefaultBiome() = BiomeField
	
	override fun calculateCelestialAngle(worldTicks: Long, partialTicks: Float): Float {
		return if (RagnarokHandler.ragnarok) 0.5f
		else super.calculateCelestialAngle(worldTicks, partialTicks)
	}
	
	override fun getBiomeGenForCoords(x: Int, z: Int): BiomeGenBase {
		return if (ASJUtilities.isClient) BiomeField else super.getBiomeGenForCoords(x, z)
	}
	
	override fun setSpawnPoint(x: Int, y: Int, z: Int) {
		if (ASJUtilities.isServer) worldObj.customData.spawnpoint = ChunkCoordinates(x, y, z)
	}
	
	override fun getSpawnPoint() = worldObj.customData.spawnpoint ?: if (enableElvenStory) ChunkCoordinates(0, 2, 0) else ChunkCoordinates(0, 220, -3)
	override fun getEntrancePortalLocation() = spawnPoint
	override fun getRandomizedSpawnPoint() = spawnPoint
	override fun canRespawnHere() = enableElvenStory || enableAlfheimRespawn
	override fun getSkyRenderer() = SkyRendererAlfheim
	override fun getWeatherRenderer() = WeatherRendererAlfheim
	override fun getCloudHeight() = 164f
	override fun isSurfaceWorld() = true
	override fun shouldMapSpin(entity: String?, x: Double, y: Double, z: Double) = false
	override fun getDimensionName() = "Alfheim"
	override fun getWorldHasVoidParticles() = false
}
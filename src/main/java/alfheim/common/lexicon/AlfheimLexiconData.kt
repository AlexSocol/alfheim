@file:Suppress("DuplicatedCode")

package alfheim.common.lexicon

import alexsocol.asjlib.*
import alexsocol.asjlib.extendables.block.BlockModMeta
import alfheim.AlfheimCore
import alfheim.api.*
import alfheim.api.crafting.recipe.TunerIncantation
import alfheim.client.core.handler.CardinalSystemClient.PlayerSegmentClient
import alfheim.common.achievement.AlfheimAchievements
import alfheim.common.block.*
import alfheim.common.block.tile.*
import alfheim.common.core.handler.AlfheimConfigHandler
import alfheim.common.core.handler.CardinalSystem.KnowledgeSystem.Knowledge
import alfheim.common.core.helper.ElementalDamage
import alfheim.common.crafting.recipe.*
import alfheim.common.entity.EntityElementalSlime
import alfheim.common.integration.thaumcraft.ThaumcraftSuffusionRecipes
import alfheim.common.integration.tinkersconstruct.TinkersConstructAlfheimConfig
import alfheim.common.item.*
import alfheim.common.item.block.*
import alfheim.common.item.material.*
import alfheim.common.item.material.ElvenFoodMetas.*
import alfheim.common.item.material.ElvenResourcesMetas.*
import alfheim.common.lexicon.AlfheimLexiconEntry.Companion.setIcon
import alfheim.common.lexicon.page.*
import net.minecraft.entity.Entity
import net.minecraft.init.Items
import net.minecraft.item.ItemStack
import tconstruct.tools.TinkerTools
import thaumcraft.common.config.ConfigItems
import vazkii.botania.api.BotaniaAPI.*
import vazkii.botania.api.lexicon.*
import vazkii.botania.common.block.ModBlocks
import vazkii.botania.common.brew.ModBrews
import vazkii.botania.common.core.handler.ConfigHandler
import vazkii.botania.common.crafting.ModCraftingRecipes
import vazkii.botania.common.item.ModItems
import vazkii.botania.common.lexicon.LexiconData
import vazkii.botania.common.lexicon.page.*
import vazkii.botania.common.lib.LibOreDict

object AlfheimLexiconData {
	
	lateinit var categoryAlfheim: LexiconCategory
	lateinit var categoryDendrology: AlfheimLexiconCategory
	lateinit var categoryDivinity: LexiconCategory
	lateinit var categoryEvents: LexiconCategory
	
	// Main addon content
	lateinit var advancedMana: LexiconEntry
	lateinit var akashic: LexiconEntry
	lateinit var alfheim: LexiconEntry
	lateinit var amplifier: LexiconEntry
	lateinit var amuletCirus: LexiconEntry
	lateinit var amuletIceberg: LexiconEntry
	lateinit var amuletNimbus: LexiconEntry
	lateinit var amulterCrescent: LexiconEntry
	lateinit var animatedTorch: LexiconEntry
	lateinit var anomaly: LexiconEntry
	lateinit var anomalyHarvester: LexiconEntry
	lateinit var anyavil: LexiconEntry
	lateinit var astrolabe: LexiconEntry
	lateinit var armilla: LexiconEntry
	lateinit var aurora: LexiconEntry
	lateinit var barrierSapling: LexiconEntry
	lateinit var beltRation: LexiconEntry
	lateinit var calicoSapling: LexiconEntry
	lateinit var carver: LexiconEntry
	lateinit var chakramEnder: LexiconEntry
	lateinit var chakramThunder: LexiconEntry
	lateinit var chalk: LexiconEntry
	lateinit var circuitSapling: LexiconEntry
	lateinit var cloakInvis: LexiconEntry
	lateinit var coatOfArms: LexiconEntry
	lateinit var colorOverride: LexiconEntry
	lateinit var coloredDirt: LexiconEntry
	lateinit var corpInj: LexiconEntry
	lateinit var corpSeq: LexiconEntry
	lateinit var dagger: LexiconEntry
	lateinit var dasGold: LexiconEntry
	lateinit var daolos: LexiconEntry
	lateinit var deathSeed: LexiconEntry
	lateinit var disguiseBelt: LexiconEntry
	lateinit var dumbDecor: LexiconEntry
	lateinit var elementalSet: LexiconEntry
	lateinit var elementalTuning: LexiconEntry
	lateinit var elvenSet: LexiconEntry
	lateinit var elvorium: LexiconEntry
	lateinit var enderAct: LexiconEntry
	lateinit var elves: LexiconEntry
	lateinit var essences: LexiconEntry
	lateinit var excaliber: LexiconEntry
	lateinit var fenrir: LexiconEntry
	lateinit var fenrirCloak: LexiconEntry
	lateinit var fenrirDrop: LexiconEntry
	lateinit var fenrirGlove: LexiconEntry
	lateinit var flowerAconite: LexiconEntry
	lateinit var flowerAlfchid: LexiconEntry
	lateinit var flowerAquapanthus: LexiconEntry
	lateinit var flowerBud: LexiconEntry
	lateinit var flowerCrysanthermum: LexiconEntry
	lateinit var flowerEnderchid: LexiconEntry
	lateinit var flowerPetronia: LexiconEntry
	lateinit var flowerRain: LexiconEntry
	lateinit var flowerSnow: LexiconEntry
	lateinit var flowerStorm: LexiconEntry
	lateinit var flowerTradescantia: LexiconEntry
	lateinit var flowerWind: LexiconEntry
	lateinit var flugel: LexiconEntry
	lateinit var fracturedSpace: LexiconEntry
	lateinit var frozenStar: LexiconEntry
	lateinit var gaiaButton: LexiconEntry
	lateinit var gjallarhorn: LexiconEntry
	lateinit var gleipnir: LexiconEntry
	lateinit var goddessCharm: LexiconEntry
	lateinit var gungnir: LexiconEntry
	lateinit var hyperBucket: LexiconEntry
	lateinit var infuser: LexiconEntry
	lateinit var irisSapling: LexiconEntry
	lateinit var itemDisplay: LexiconEntry
	lateinit var ivySave: LexiconEntry
	lateinit var kindling: LexiconEntry
	lateinit var lamp: LexiconEntry
	lateinit var legends: LexiconEntry
	lateinit var lembas: LexiconEntry
	lateinit var lightningSapling: LexiconEntry
	lateinit var livingwoodFunnel: LexiconEntry
	lateinit var lootInt: LexiconEntry
	lateinit var manaAccelerator: LexiconEntry
	lateinit var manaImba: LexiconEntry
	lateinit var manaLamp: LexiconEntry
	lateinit var manaReflector: LexiconEntry
	lateinit var manaTuner: LexiconEntry
	lateinit var mask: LexiconEntry
	lateinit var mitten: LexiconEntry
	lateinit var mjolnir: LexiconEntry
	lateinit var mobs: LexiconEntry
	lateinit var moonbow: LexiconEntry
	lateinit var multbauble: LexiconEntry
	lateinit var netherSapling: LexiconEntry
	lateinit var nidhoggTooth: LexiconEntry
	lateinit var openChest: LexiconEntry
	lateinit var ores: LexiconEntry
	lateinit var pastoralSeeds: LexiconEntry
	lateinit var pixie: LexiconEntry
	lateinit var portal: LexiconEntry
	lateinit var pylons: LexiconEntry
	lateinit var corpQuandex: LexiconEntry
	lateinit var rainbowFlora: LexiconEntry
	lateinit var flowerRattlerose: LexiconEntry
	lateinit var reality: LexiconEntry
	lateinit var redstoneRelay: LexiconEntry
	lateinit var resonator: LexiconEntry
	lateinit var ringsAura: LexiconEntry
	lateinit var ringAnomaly: LexiconEntry
	lateinit var ringDodge: LexiconEntry
	lateinit var ringManaDrive: LexiconEntry
	lateinit var ringSpider: LexiconEntry
	lateinit var rodClick: LexiconEntry
	lateinit var rodGreen: LexiconEntry
	lateinit var rodPrismatic: LexiconEntry
	lateinit var rodRedstone: LexiconEntry
	lateinit var rodSuperExchange: LexiconEntry
	lateinit var ruling: LexiconEntry
	lateinit var runes: LexiconEntry
	lateinit var sealCreepers: LexiconEntry
	lateinit var serenade: LexiconEntry
	lateinit var shimmer: LexiconEntry
	lateinit var shrines: LexiconEntry
	lateinit var silencer: LexiconEntry
	lateinit var slimes: LexiconEntry
	lateinit var soul: LexiconEntry
	lateinit var soulHorn: LexiconEntry
	lateinit var soulSword: LexiconEntry
	lateinit var specialAxe: LexiconEntry
	lateinit var subshroom: LexiconEntry
	lateinit var subspear: LexiconEntry
	lateinit var tctrees: LexiconEntry
	lateinit var temperature: LexiconEntry
	lateinit var terraHarvester: LexiconEntry
	lateinit var throwablePotions: LexiconEntry
	lateinit var toolbelt: LexiconEntry
	lateinit var trade: LexiconEntry
	lateinit var treeBerry: LexiconEntry
	lateinit var treeCrafting: LexiconEntry
	lateinit var triquetrum: LexiconEntry
	lateinit var tunedSaplings: LexiconEntry
	lateinit var uberSpreader: LexiconEntry
	lateinit var warBanner: LexiconEntry
	lateinit var winery: LexiconEntry
	lateinit var worldgen: LexiconEntry
	lateinit var worldTree: LexiconEntry
	
	// Elven Story information
	var esm: LexiconEntry? = null
	var races: LexiconEntry? = null
	
	// MMO info
	var parties: LexiconEntry? = null
	var spells: LexiconEntry? = null
	var targets: LexiconEntry? = null
	
	// divinity
	lateinit var divIntro: LexiconEntry
	lateinit var abyss: LexiconEntry
	lateinit var vafthrudnir: LexiconEntry
	
	lateinit var cloakThor: LexiconEntry
	lateinit var cloakSif: LexiconEntry
	lateinit var cloakNjord: LexiconEntry
	lateinit var cloakLoki: LexiconEntry
	lateinit var cloakHeimdall: LexiconEntry
	lateinit var cloakOdin: LexiconEntry
	
	lateinit var emblemThor: LexiconEntry
	lateinit var emblemSif: LexiconEntry
	lateinit var emblemNjord: LexiconEntry
	lateinit var emblemLoki: LexiconEntry
	lateinit var emblemHeimdall: LexiconEntry
	lateinit var emblemOdin: LexiconEntry
	
	lateinit var ringSif: LexiconEntry
	lateinit var ringNjord: LexiconEntry
	lateinit var ringHeimdall: LexiconEntry
	
	lateinit var rodThor: LexiconEntry
	lateinit var rodSif: LexiconEntry
	lateinit var rodNjord: LexiconEntry
	lateinit var rodLoki: LexiconEntry
	lateinit var rodOdin: LexiconEntry
	
	lateinit var HV: LexiconEntry
	lateinit var WOTW: LexiconEntry
	
	fun preInit() {
		categoryAlfheim = AlfheimLexiconCategory("Alfheim", 5)
		categoryDivinity = AlfheimLexiconCategory("Divinity", 5)
		categoryDendrology = AlfheimLexiconCategory("dendrology", 1)
		categoryEvents = AlfheimLexiconCategory("events", 4)
		
		val alfomancy = categoryAlfhomancy
		val baubles = categoryBaubles
		val enderArtefacts = categoryEnder
		val functionalFlora = categoryFunctionalFlowers
		val generatingFlora = categoryGenerationFlowers
		val manaManipulation = categoryMana
		val miscellaneous = categoryMisc
		val mysticalItems = categoryTools
		val naturalApparatus = categoryDevices
		
		advancedMana = AlfheimLexiconEntry("advMana", pickCategory(manaManipulation))
		alfheim = AlfheimLexiconEntry("alfheim", categoryAlfheim)
		amplifier = AlfheimLexiconEntry("amplifier", pickCategory(miscellaneous))
		amuletCirus = AlfheimLexiconEntry("amulCirs", pickCategory(baubles))
		amulterCrescent = AlfheimLexiconEntry("crescent", pickCategory(baubles))
		amuletIceberg = AlfheimLexiconEntry("iceberg", pickCategory(baubles))
		amuletNimbus = AlfheimLexiconEntry("amulNimb", pickCategory(baubles))
		animatedTorch = AlfheimLexiconEntry("aniTorch", pickCategory(naturalApparatus))
		anomaly = AlfheimLexiconEntry("anomaly", categoryAlfheim)
		anomalyHarvester = AlfheimLexiconEntry("anomalyHarvester", pickCategory(naturalApparatus))
		anyavil = AlfheimLexiconEntry("anyavil", pickCategory(naturalApparatus))
		astrolabe = AlfheimLexiconEntry("astrolab", pickCategory(mysticalItems))
		armilla = AlfheimLexiconEntry("armilla", pickCategory(mysticalItems))
		aurora = AlfheimLexiconEntry("aurora", pickCategory(miscellaneous))
		barrierSapling = AlfheimLexiconEntry("barrierSapling", categoryDendrology)
		beltRation = AlfheimLexiconEntry("ration", pickCategory(baubles))
		calicoSapling = AlfheimLexiconEntry("calicoSapling", categoryDendrology)
		carver = AlfheimLexiconEntry("carver", pickCategory(mysticalItems))
		chakramEnder = AlfheimLexiconEntry("chakramEnder", pickCategory(enderArtefacts))
		chakramThunder = AlfheimLexiconEntry("chakramThunder", pickCategory(mysticalItems))
		chalk = AlfheimLexiconEntry("chalk", pickCategory(mysticalItems))
		circuitSapling = AlfheimLexiconEntry("circuitSapling", categoryDendrology)
		cloakInvis = AlfheimLexiconEntry("cloakInv", pickCategory(baubles))
		coatOfArms = AlfheimLexiconEntry("coatOfArms", pickCategory(baubles))
		coloredDirt = AlfheimLexiconEntry("coloredDirt", pickCategory(miscellaneous))
		colorOverride = AlfheimLexiconEntry("colorOverride", pickCategory(baubles))
		corpInj = AlfheimLexiconEntry("corpInj", pickCategory(enderArtefacts))
		corpQuandex = AlfheimLexiconEntry("corpQuandex", pickCategory(enderArtefacts))
		corpSeq = AlfheimLexiconEntry("corpSeq", pickCategory(enderArtefacts))
		dagger = AlfheimRelicLexiconEntry("dagger", pickCategory(alfomancy))
		dasGold = AlfheimLexiconEntry("dasGold", pickCategory(alfomancy))
		deathSeed = AlfheimLexiconEntry("deathSeed", pickCategory(enderArtefacts))
		disguiseBelt = AlfheimLexiconEntry("disguiseBelt", pickCategory(baubles))
		dumbDecor = AlfheimLexiconEntry("dumbDecor", pickCategory(miscellaneous))
		elementalSet = AlfheimLexiconEntry("elemSet", pickCategory(mysticalItems))
		elementalTuning = AlfheimLexiconEntry("elementalTuning", pickCategory(miscellaneous))
		elvenSet = AlfheimLexiconEntry("elvenSet", pickCategory(mysticalItems))
		elves = AlfheimLexiconEntry("elves", categoryAlfheim)
		enderAct = AlfheimLexiconEntry("endAct", pickCategory(enderArtefacts))
		essences = AlfheimLexiconEntry("essences", pickCategory(alfomancy))
		elvorium = AlfheimLexiconEntry("elvorium", pickCategory(categoryBasics))
		fenrir = AlfheimLexiconEntry("fenrir", mysticalItems)
		fenrirCloak = AlfheimLexiconEntry("fenrirCloak", pickCategory(baubles))
		fenrirDrop = AlfheimLexiconEntry("fenrirDrop", mysticalItems)
		fenrirGlove = AlfheimLexiconEntry("fenrirGlove", pickCategory(baubles))
		flowerAconite = AlfheimLexiconEntry("aconite", pickCategory(generatingFlora))
		flowerAlfchid = AlfheimLexiconEntry("flowerAlfchid", pickCategory(functionalFlora))
		flowerAquapanthus = AlfheimLexiconEntry("aquapanthus", pickCategory(functionalFlora))
		flowerBud = AlfheimLexiconEntry("bud", pickCategory(functionalFlora))
		flowerCrysanthermum = AlfheimLexiconEntry("crysanthermum", pickCategory(generatingFlora))
		flowerEnderchid = AlfheimLexiconEntry("flowerEnderchid", pickCategory(functionalFlora))
		flowerPetronia = AlfheimLexiconEntry("flowerPetronia", pickCategory(generatingFlora))
		flowerRain = AlfheimLexiconEntry("flowerRain", pickCategory(generatingFlora))
		flowerRattlerose = AlfheimLexiconEntry("flowerRattlerose", pickCategory(generatingFlora))
		flowerSnow = AlfheimLexiconEntry("flowerSnow", pickCategory(generatingFlora))
		flowerStorm = AlfheimLexiconEntry("flowerStorm", pickCategory(generatingFlora))
		flowerTradescantia = AlfheimLexiconEntry("flowerTradescantia", pickCategory(functionalFlora))
		flowerWind = AlfheimLexiconEntry("flowerWind", pickCategory(generatingFlora))
		flugel = AlfheimLexiconEntry("flugel", pickCategory(alfomancy))
		fracturedSpace = AlfheimLexiconEntry("fracturedSpace", pickCategory(enderArtefacts))
		frozenStar = AlfheimLexiconEntry("starBlock", pickCategory(miscellaneous))
		gaiaButton = AlfheimLexiconEntry("gaiaButton", pickCategory(naturalApparatus))
		goddessCharm = AlfheimLexiconEntry("goddessCharm", pickCategory(baubles))
		hyperBucket = AlfheimLexiconEntry("hyperBuk", pickCategory(mysticalItems))
		infuser = AlfheimLexiconEntry("infuser", pickCategory(categoryBasics))
		irisSapling = AlfheimLexiconEntry("irisSapling", categoryDendrology)
		itemDisplay = AlfheimLexiconEntry("itemDisplay", pickCategory(miscellaneous))
		ivySave = AlfheimLexiconEntry("ivySave", pickCategory(miscellaneous))
		kindling = AlfheimLexiconEntry("kindling", pickCategory(naturalApparatus))
		lamp = AlfheimLexiconEntry("lamp", pickCategory(miscellaneous))
		legends = AlfheimLexiconEntry("legends", categoryAlfheim)
		lembas = AlfheimLexiconEntry("lembas", categoryAlfheim)
		lightningSapling = AlfheimLexiconEntry("lightningSapling", categoryDendrology)
		livingwoodFunnel = AlfheimLexiconEntry("livingwoodFunnel", pickCategory(naturalApparatus))
		lootInt = AlfheimLexiconEntry("lootInt", pickCategory(enderArtefacts))
		manaAccelerator = AlfheimLexiconEntry("itemHold", pickCategory(manaManipulation))
		manaImba = AlfheimLexiconEntry("manaImba", pickCategory(manaManipulation))
		manaLamp = AlfheimLexiconEntry("manaLamp", pickCategory(mysticalItems))
		manaReflector = AlfheimLexiconEntry("manaReflector", pickCategory(manaManipulation))
		manaTuner = AlfheimLexiconEntry("manaTuner", pickCategory(naturalApparatus))
		mitten = AlfheimLexiconEntry("mitten", pickCategory(baubles))
		mobs = AlfheimLexiconEntry("mobs", categoryAlfheim)
		multbauble = AlfheimLexiconEntry("multbaub", pickCategory(baubles))
		netherSapling = AlfheimLexiconEntry("infernalSapling", categoryDendrology)
		nidhoggTooth = AlfheimLexiconEntry("nidhoggTooth", categoryAlfheim)
		openChest = AlfheimLexiconEntry("openChest", pickCategory(naturalApparatus))
		ores = AlfheimLexiconEntry("ores", categoryAlfheim)
		pastoralSeeds = AlfheimLexiconEntry("irisSeeds", pickCategory(miscellaneous))
		pixie = AlfheimLexiconEntry("pixie", pickCategory(baubles))
		portal = AlfheimLexiconEntry("portal", pickCategory(alfomancy))
		pylons = AlfheimLexiconEntry("pylons", pickCategory(naturalApparatus))
		rainbowFlora = AlfheimLexiconEntry("rainbowFlora", pickCategory(miscellaneous))
		reality = AlfheimLexiconEntry("reality", pickCategory(mysticalItems))
		redstoneRelay = AlfheimLexiconEntry("redstoneRelay", pickCategory(enderArtefacts))
		resonator = AlfheimLexiconEntry("resonator", pickCategory(mysticalItems))
		ringsAura = AlfheimLexiconEntry("auraAlf", pickCategory(baubles))
		ringAnomaly = AlfheimLexiconEntry("anomaRing", pickCategory(baubles))
		ringDodge = AlfheimLexiconEntry("dodgRing", pickCategory(baubles))
		ringManaDrive = AlfheimLexiconEntry("manaDrive", pickCategory(baubles))
		ringSpider = AlfheimLexiconEntry("spider", pickCategory(baubles))
		rodClick = AlfheimLexiconEntry("rodClick", pickCategory(mysticalItems))
		rodGreen = AlfheimLexiconEntry("greenRod", pickCategory(mysticalItems))
		rodPrismatic = AlfheimLexiconEntry("rodPrismatic", pickCategory(mysticalItems))
		rodRedstone = AlfheimLexiconEntry("rodRedstone", pickCategory(mysticalItems))
		rodSuperExchange = AlfheimLexiconEntry("rodSuperExchange", pickCategory(mysticalItems))
		ruling = AlfheimLexiconEntry("ruling", categoryAlfheim)
		runes = AlfheimLexiconEntry("runes", pickCategory(categoryBasics))
		sealCreepers = AlfheimLexiconEntry("sealCreepers", pickCategory(miscellaneous))
		serenade = AlfheimLexiconEntry("serenade", pickCategory(baubles))
		shimmer = AlfheimLexiconEntry("shimmer", pickCategory(miscellaneous))
		shrines = AlfheimLexiconEntry("shrines", categoryAlfheim)
		silencer = AlfheimLexiconEntry("silencer", categoryDendrology)
		slimes = AlfheimLexiconEntry("slimes", pickCategory(miscellaneous))
		soulSword = AlfheimLexiconEntry("soulSword", pickCategory(mysticalItems))
		specialAxe = AlfheimRelicLexiconEntry("andmyaxe", pickCategory(alfomancy))
		subshroom = AlfheimLexiconEntry("subshroom", categoryAlfheim)
		temperature = AlfheimLexiconEntry("temperature", pickCategory(categoryBasics))
		terraHarvester = AlfheimLexiconEntry("terraHarvester", pickCategory(mysticalItems))
		throwablePotions = AlfheimLexiconEntry("throwablePotions", pickCategory(naturalApparatus))
		toolbelt = AlfheimLexiconEntry("toolbelt", pickCategory(baubles))
		trade = AlfheimLexiconEntry("trade", categoryAlfheim)
		treeBerry = AlfheimLexiconEntry("treeBerry", categoryDendrology)
		treeCrafting = AlfheimLexiconEntry("treeCrafting", categoryDendrology)
		triquetrum = AlfheimLexiconEntry("triquetrum", pickCategory(mysticalItems))
		tunedSaplings = AlfheimLexiconEntry("tunedSaplings", categoryDendrology)
		uberSpreader = AlfheimLexiconEntry("uberSpreader", pickCategory(manaManipulation))
		warBanner = AlfheimLexiconEntry("warBanner", pickCategory(baubles))
		winery = AlfheimLexiconEntry("winery", categoryAlfheim)
		worldgen = AlfheimLexiconEntry("worldgen", categoryAlfheim)
		worldTree = AlfheimLexiconEntry("worldTree", pickCategory(naturalApparatus))
		
		
		
		divIntro = AlfheimLexiconEntry("divinity_intro", categoryDivinity)
		abyss = AlfheimAbyssalLexiconEntry("abyss", "aesir", categoryDivinity)
		vafthrudnir = object: AlfheimLexiconEntry("vafthrudnir", categoryDivinity) {
			override fun isVisible() = Knowledge.ABYSS_TRUTH.toString() in PlayerSegmentClient.knowledge
		}
		
		cloakThor = AlfheimLexiconEntry("garb_thor", categoryDivinity)
		cloakSif = AlfheimLexiconEntry("garb_sif", categoryDivinity)
		cloakNjord = AlfheimLexiconEntry("garb_njord", categoryDivinity)
		cloakLoki = AlfheimLexiconEntry("garb_loki", categoryDivinity)
		cloakHeimdall = AlfheimLexiconEntry("garb_heimdall", categoryDivinity)
		cloakOdin = AlfheimLexiconEntry("garb_odin", categoryDivinity)
		
		emblemThor = AlfheimLexiconEntry("thor", categoryDivinity)
		emblemSif = AlfheimLexiconEntry("sif", categoryDivinity)
		emblemNjord = AlfheimLexiconEntry("njord", categoryDivinity)
		emblemLoki = AlfheimLexiconEntry("loki", categoryDivinity)
		emblemHeimdall = AlfheimLexiconEntry("heimdall", categoryDivinity)
		emblemOdin = AlfheimLexiconEntry("odin", categoryDivinity)
		
		rodThor = AlfheimLexiconEntry("rod_thor", categoryDivinity)
		rodSif = AlfheimLexiconEntry("rod_sif", categoryDivinity)
		rodNjord = AlfheimLexiconEntry("rod_njord", categoryDivinity)
		rodLoki = AlfheimLexiconEntry("rod_loki", categoryDivinity)
		rodOdin = AlfheimLexiconEntry("rod_odin", categoryDivinity)
		
		HV = AlfheimLexiconEntry("HV", categoryEvents)
		WOTW = AlfheimLexiconEntry("WOTW", categoryEvents)
		
		if (AlfheimConfigHandler.enableElvenStory)
			preInitElvenStory()
	}
	
	private fun preInitElvenStory() {
		if (esm == null) esm = AlfheimLexiconEntry("es", categoryAlfheim)
		if (races == null) races = AlfheimLexiconEntry("races", categoryAlfheim)
		
		if (AlfheimConfigHandler.enableMMO)
			preInitMMO()
	}
	
	private fun preInitMMO() {
		if (parties == null) parties = AlfheimLexiconEntry("parties", categoryAlfheim)
		if (spells == null) spells = AlfheimLexiconEntry("spells", categoryAlfheim)
		if (targets == null) targets = AlfheimLexiconEntry("targets", categoryAlfheim)
	}
	
	fun init() {
		advancedMana.setLexiconPages(PageText("0"), PageText("1"),
		                             PageManaInfuserRecipe("2", AlfheimRecipes.recipeManaStone),
		                             PageManaInfuserRecipe("3", AlfheimRecipes.recipeManaStoneGreater),
		                             PageText("4"),
		                             PageCraftingRecipe("5", AlfheimRecipes.recipeManaRingPink),
		                             PageCraftingRecipe("6", AlfheimRecipes.recipeManaRingElven),
		                             PageCraftingRecipe("7", AlfheimRecipes.recipeManaRingGod))
					.setIcon(AlfheimItems.manaStone)
		
		alfheim.setLexiconPages(PageText("0"), PageText("1")).setPriority()
		
		amplifier.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeAmplifier))
				 .setIcon(AlfheimBlocks.amplifier)
		
		amuletCirus.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCloudPendant))
		
		amulterCrescent.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCrescentAmulet))
		
		amuletIceberg.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipePendantSuperIce))
		
		amuletNimbus.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCloudPendantSuper))
		
		animatedTorch.setLexiconPages(PageText("0"), PageText("1"), PageText("2"), PageCraftingRecipe("3", AlfheimRecipes.recipeAnimatedTorch))
		
		val anomalyIcon = ItemBlockAnomaly.ofType("Lightning")
		
		anomaly.setLexiconPages(PageText("0"))
		for (name in AlfheimAPI.anomalies.keys) {
			anomaly.setLexiconPages(
				PageImage("$name.t", ModInfo.MODID + ":textures/gui/entries/Anomaly" + name + ".png"),
				if (name != "Lightning")
					PageText("$name.d")
				else
					PageTextLearnableKnowledge("$name.d", Knowledge.PYLONS))
			
			LexiconRecipeMappings.map(ItemBlockAnomaly.ofType(name), anomaly, anomaly.pages.size - 1)
		}
		anomaly.icon = anomalyIcon
		
		val anomalyIcons = AlfheimAPI.anomalies.keys.map { ItemBlockAnomaly.ofType(it) }.toTypedArray()
		val stableAnomalyIcons = anomalyIcons.map { ai -> ai.copy().also { ItemNBTHelper.setBoolean(it, TileAnomaly.TAG_STABLE, true) } }.toTypedArray()
		val riftDriveIcons = anomalyIcons.map { ai -> RiftDrive.stack.also { ItemNBTHelper.setString(it, TileAnomaly.TAG_SUBTILE_NAME, ItemBlockAnomaly.getType(ai)) } }.toTypedArray()
		
		anomalyHarvester.setLexiconPages(*Array(3) { PageText("$it") },
		                                 PageCraftingRecipe("3", AlfheimRecipes.recipeAnomalyHarvester),
		                                 PageTuningRecipe("4", AlfheimRecipes.tuningAnomalyStabilization, anomalyIcons, stableAnomalyIcons),
		                                 PageTuningRecipe("5", AlfheimRecipes.tuningAnomalyPackaging, stableAnomalyIcons, riftDriveIcons),
		                                 PageText("6"), PageText("7"))
		LexiconRecipeMappings.map(RiftDrive.stack, anomalyHarvester, 5)
		
		anyavil.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeAnyavil))
		
		astrolabe.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeAstrolabe))
		
		armilla.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeArmilla))
		
		aurora.setLexiconPages(PageText("0"),
							   PageCraftingRecipe("1", AlfheimRecipes.recipeAuroraDirt),
							   PageCraftingRecipe("2", AlfheimRecipes.recipeAuroraPlanks),
							   PageCraftingRecipe("3", AlfheimRecipes.recipeAuroraStairs),
							   PageCraftingRecipe("4", AlfheimRecipes.recipeAuroraSlabs),
							   PageCraftingRecipe("5", AlfheimRecipes.recipeAuroraPlanksFromSlabs))
				.setIcon(AlfheimBlocks.auroraDirt)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.auroraLeaves), aurora, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.auroraWood), aurora, 0)
		
		beltRation.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRationBelt))
		
		carver.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCarver))
		
		chakramEnder.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeChakramEnder))
		
		chakramThunder.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeChakramThunder))
		
		chalk.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeRunicChalk))
		
		cloakInvis.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeInvisibilityCloak))
		
		coatOfArms.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipesCoatOfArms)).icon = ItemStack(AlfheimItems.coatOfArms, 1, 16)
		
		for (i in 0..18)
			LexiconRecipeMappings.map(ItemStack(AlfheimItems.coatOfArms, 1, i), coatOfArms, 1)
		
		coloredDirt.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipesColoredDirt))
				.setIcon(AlfheimBlocks.rainbowDirt)
		
		for (i in 0..15)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisDirt, 1, i), coloredDirt, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowDirt), coloredDirt, 1)
		
		colorOverride.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeColorOverride)).setIcon(AlfheimItems.colorOverride)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.colorOverride), colorOverride, 1)
		
		corpInj.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeInjector))
		
		corpQuandex.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeQuandexBase), PageCraftingRecipe("3", AlfheimRecipes.recipeQuandex))
		
		corpSeq.setLexiconPages(PageText("0"), PageText("1"), PageText("2"), PageCraftingRecipe("3", AlfheimRecipes.recipeAutocrafter))
		
		dagger.setLexiconPages(PageText("0")).icon = ItemStack(AlfheimItems.trisDagger)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.trisDagger), dagger, 0)
		
		dasGold.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRelicCleaner))
		
		deathSeed.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeDeathSeed))
		
		disguiseBelt.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeDisguiseBelt))
		
		dumbDecor.setLexiconPages(*Array(2) { PageText("$it") },
		                          PageCraftingRecipe("2", AlfheimRecipes.recipesDecor),
		                          PageText("3"),
		                          PageCraftingRecipe("4", AlfheimRecipes.recipesDecorCurtain),
		                          PageText("5"),
		                          PageCraftingRecipe("6", AlfheimRecipes.recipesDecorDouble),
		                          PageText("7"),
		                          PageCraftingRecipe("8", AlfheimRecipes.recipesDecorGlass),
		                          PageText("9"),
		                          PageCraftingRecipe("10", AlfheimRecipes.recipesDecorLight)).icon = ItemStack(AlfheimFluffBlocks.floodLight, 1, 3)
		
		elementalSet.setLexiconPages(PageText("0"),
									 PageCraftingRecipe("1", AlfheimRecipes.recipeElementalHelmet),
									 PageCraftingRecipe("2", AlfheimRecipes.recipeElementalChestplate),
									 PageCraftingRecipe("3", AlfheimRecipes.recipeElementalLeggings),
									 PageCraftingRecipe("4", AlfheimRecipes.recipeElementalBoots)).icon = ItemStack(AlfheimItems.elementalHelmet)
		AlfheimItems.elementalHelmetRevealing?.let { elementalSet.addExtraDisplayedRecipe(ItemStack(it)) }
		
		elementalTuning.setLexiconPages(*Array(3) { PageText("$it") },
		                                PageTuningRecipe("3", AlfheimRecipes.tuningElementalSeer, ItemStack(ModItems.monocle)),
			// WARNING! Dirty hacks for recipe display:
			                            *AlfheimAPI.tunerIncantations.values().filterIsInstance<IncantationEquipmentElementalTuning>().map {
				                            PageTuningRecipe("${it.index + 4}",
				                                             TunerIncantation(Entity::class.java,
				                                                              it.incantation,
				                                                              arrayOf(
					                                                              *Array(7) { LibOreDict.MANAWEAVE_CLOTH },
					                                                              ItemElvenResource.ballForElement(ElementalDamage.valueOf(it.element))
				                                                              )
				                                             ) { false },
				                                             ItemStack(ModItems.manasteelSword)
				                            )
			                            }.toTypedArray()).icon = ItemElvenResource.ballForElement(null)
		LexiconRecipeMappings.map(ElementalSlimeBall.stack, elementalTuning, 3)
		
		elvenSet.setLexiconPages(PageText("0"),
								 PageCraftingRecipe("1", AlfheimRecipes.recipeElvoriumHelmet),
								 PageCraftingRecipe("2", AlfheimRecipes.recipeElvoriumChestplate),
								 PageCraftingRecipe("3", AlfheimRecipes.recipeElvoriumLeggings),
								 PageCraftingRecipe("4", AlfheimRecipes.recipeElvoriumBoots)).icon = ItemStack(AlfheimItems.elvoriumHelmet)
		AlfheimItems.elvoriumHelmetRevealing?.let { elvenSet.addExtraDisplayedRecipe(ItemStack(it)) }
		
		elves.setLexiconPages(*Array(5) { PageText("$it") }).setPriority()
		
		elvorium.setLexiconPages(PageText("0"), PageManaInfuserRecipe("1", AlfheimRecipes.recipeElvorium)).icon = ElvoriumIngot.stack
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.alfStorage, 1, 0), elvorium, 0)
		LexiconRecipeMappings.map(ElvoriumNugget.stack, elvorium, 1)
		
		enderAct.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeEnderActuator))
		
		essences.setLexiconPages(PageText("0"),
		                         PageTextLearnableAchievement("2", AlfheimAchievements.flugelHardKill),
		                         PageText("4"), PageText("5"), PageCraftingRecipe("6", listOf(AlfheimRecipes.recipeMuspelheimPowerIngot, AlfheimRecipes.recipeNiflheimPowerIngot)),
		                         PageText("7"), PageManaInfuserRecipe("8", AlfheimRecipes.recipeMauftrium)).icon = ItemStack(ModItems.manaResource, 1, 5)
		essences.addExtraDisplayedRecipe(NiflheimPowerIngot.stack)
		essences.addExtraDisplayedRecipe(NiflheimEssence.stack)
		essences.addExtraDisplayedRecipe(MuspelheimEssence.stack)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.alfStorage, 1, 1), essences, 6)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.alfStorage, 1, 2), essences, 4)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.alfStorage, 1, 3), essences, 4)
		LexiconRecipeMappings.map(MuspelheimEssence.stack, essences, 1)
		LexiconRecipeMappings.map(NiflheimEssence.stack, essences, 1)
		LexiconRecipeMappings.map(MauftriumNugget.stack, essences, 6)
		
		fenrir.setLexiconPages(PageText("0"), PageText("1"),
		                       PageCraftingRecipe("2", AlfheimRecipes.recipeFenrirHelmet),
		                       PageCraftingRecipe("3", AlfheimRecipes.recipeFenrirChestplate),
		                       PageCraftingRecipe("4", AlfheimRecipes.recipeFenrirLeggings),
		                       PageCraftingRecipe("5", AlfheimRecipes.recipeFenrirBoots)).setIcon(AlfheimItems.fenrirHelmet)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.fenrirClaws), fenrir, 1)
		LexiconRecipeMappings.map(FenrirFur.stack, fenrir, 0)
		
		fenrirCloak.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeFenrirCloak))
		
		fenrirDrop.setLexiconPages(*Array(6) { PageText("$it") })
		ItemFenrirLoot.FenrirLootMetas.entries.forEach {
			LexiconRecipeMappings.map(it.stack, fenrirDrop, it.ordinal + 1)
		}
		
		fenrirGlove.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeFenrirGlove))
		
		flowerAconite.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipeWitherAconite))
		flowerAlfchid.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipeOrechidAlfarem))
		flowerAquapanthus.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipeAquapanthus))
		flowerBud.setLexiconPages(PageText("0"), PageText("1"), PagePetalRecipe("2", AlfheimRecipes.recipeBud))
		flowerCrysanthermum.setLexiconPages(PageText("0"), PageText("1"), PageText("2"), PagePetalRecipe("3", AlfheimRecipes.recipeCrysanthermum))
		flowerEnderchid.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipeOrechidEndium)).icon = internalHandler.getSubTileAsStack("orechidEndium")
		flowerPetronia.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipePetronia)).icon = internalHandler.getSubTileAsStack("petronia")
		flowerRain.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipeRainFlower)).icon = internalHandler.getSubTileAsStack("rainFlower")
		flowerRattlerose.setLexiconPages(*Array(5) { PageText("$it") }, PagePetalRecipe("5", AlfheimRecipes.recipeRattlerose), PageRuneRecipe("6", AlfheimRecipes.recipeSnakeEgg)).icon = internalHandler.getSubTileAsStack("rattlerose")
		flowerSnow.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipeSnowFlower)).icon = internalHandler.getSubTileAsStack("snowFlower")
		flowerStorm.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipeStormFlower)).icon = internalHandler.getSubTileAsStack("stormFlower")
		flowerTradescantia.setLexiconPages(PageText("0"), PageText("1"), PagePetalRecipe("2", AlfheimRecipes.recipeTradescantia)).icon = internalHandler.getSubTileAsStack("tradescantia")
		flowerWind.setLexiconPages(PageText("0"), PagePetalRecipe("1", AlfheimRecipes.recipeWindFlower)).icon = internalHandler.getSubTileAsStack("windFlower")
		
		for (i in 0..15)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.snakeBody, 1, i), flowerRattlerose, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.snakeObject, 1, 1), flowerRattlerose, 0)
		
		flugel.setLexiconPages(*Array(3) { PageText("$it") }).icon = ItemStack(ModItems.flightTiara, 1, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.discFlugel), flugel, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.discFlugelMeme), flugel, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.discFlugelUltra), flugel, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.flugelHead), flugel, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.flugelHead2), flugel, 0)
		
		fracturedSpace.setLexiconPages(*Array(3) { PageText("$it") }, PageCraftingRecipe("3", AlfheimRecipes.recipeRodBlackhole))
		
		frozenStar.setLexiconPages(PageText("0"),
								   PageCraftingRecipe("1", AlfheimRecipes.recipesStar),
								   PageText("2"),
								   PageCraftingRecipe("3", AlfheimRecipes.recipesStar2)).icon = ItemStarPlacer.forColor(16)
		
		gaiaButton.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeGaiaButton))
		
		goddessCharm.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeGoddessCharm))
		
		hyperBucket.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeHyperBucket))
		
		irisSapling.setLexiconPages(PageText("0"),
		                            PagePureDaisyRecipe("1", AlfheimRecipes.recipeIrisSapling),
									PageCraftingRecipe("2", AlfheimRecipes.recipesColoredPlanks + AlfheimRecipes.recipesAltPlanks),
									PageCraftingRecipe("3", AlfheimRecipes.recipesColoredStairs + AlfheimRecipes.recipesAltStairs),
									PageCraftingRecipe("4", AlfheimRecipes.recipesColoredSlabs + AlfheimRecipes.recipesAltSlabs),
									PageCraftingRecipe("5", AlfheimRecipes.recipesColoredPlanksFromSlabs + AlfheimRecipes.recipesAltPlanksFromSlabs),
									PageCraftingRecipe("6", AlfheimRecipes.recipesLeafDyes))
			.setPriority().setIcon(AlfheimBlocks.irisSapling)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisSapling), irisSapling, 0)
		
		for (i in 0..3) {
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisWood0, 1, i), irisSapling, 0)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisWood1, 1, i), irisSapling, 0)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisWood2, 1, i), irisSapling, 0)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisWood3, 1, i), irisSapling, 0)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.altWood0, 1, i), irisSapling, 0)
		}
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.altWood1, 1, 0), irisSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.altWood1, 1, 1), irisSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowWood), irisSapling, 0)
		
		for (i in 0..15) {
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisPlanks, 1, i), irisSapling, 1)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisSlabs[i], 1), irisSapling, 2)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisStairs[i], 1), irisSapling, 3)
		}
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowPlanks), irisSapling, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowSlab), irisSapling, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowStairs), irisSapling, 3)
		
		for (i in 0..7) {
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisLeaves0, 1, i), irisSapling, 0)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisLeaves1, 1, i), irisSapling, 0)
		}
		for (i in 0..5) {
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.altLeaves, 1, i), irisSapling, 0)
		}
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowLeaves), irisSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowLeaves), irisSapling, 0)
		
		infuser.setLexiconPages(PageText("0"), PageText("1"),
								PageCraftingRecipe("2", AlfheimRecipes.recipeManaInfusionCore),
								PageCraftingRecipe("3", AlfheimRecipes.recipeManaInfuser),
								PageText("4"),
								PageMultiblockLearnable("5", AlfheimMultiblocks.infuserU, AlfheimMultiblocks.infuser, AlfheimAchievements.infuser)).setIcon(AlfheimBlocks.manaInfuser)
		
		itemDisplay.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipesItemDisplay)).icon = ItemStack(AlfheimBlocks.itemDisplay, 1, 1)
		for (i in 0..BlockItemDisplay.TYPES)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.itemDisplay, 1, i), itemDisplay, 1)
		
		ivySave.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeSaveIvy))
		
		kindling.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeKindling))
		
		lamp.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeLamp))
		
		legends.setLexiconPages(*Array(6) { PageText("$it") }).setPriority()
		LexiconRecipeMappings.map(YggFruit.stack, legends, 1)
		legends.icon = null
		
		lembas.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeLembas))
		
		livingwoodFunnel.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeLivingwoodFunnel)).setIcon(AlfheimBlocks.livingwoodFunnel)
		
		lootInt.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeLootInterceptor))
		
		manaAccelerator.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeManaAccelerator))
		
		manaReflector.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeManaReflector))
		
		manaImba.setLexiconPages(*Array(3) { PageText("$it") }, PageCraftingRecipe("3", AlfheimRecipes.recipeManaMirrorImba))
		
		manaLamp.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeEnlighter))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.enlighter, 1, 1), manaLamp, 1)
		
		manaTuner.setLexiconPages(*Array(3) { PageText("$it") },
		                          PageCraftingRecipe("3", AlfheimRecipes.recipeManaTuner),
		                          PageText("4"),
		                          PageTunerCodes)
		
		mitten.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeManaweaveGlove))
		
		mobs.setLexiconPages(*Array(6) { PageText("$it") },
		                    PageCraftingRecipe("6", AlfheimRecipes.recipeJellybread),
		                    PageCraftingRecipe("7", AlfheimRecipes.recipeJellyfish))
			.icon = ItemStack(ModItems.manaResource, 1, 8)
		
		LexiconRecipeMappings.map(Nectar.stack, mobs, 2)
		LexiconRecipeMappings.map(JellyBottle.stack, mobs, 3)
		
		multbauble.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeMultibauble))
		
		nidhoggTooth.setLexiconPages(PageText("0"), PageText("1"))
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.nidhoggTooth), nidhoggTooth, 0)
		
		openChest.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeOpenChest))
		
		ores.setLexiconPages(*Array(3) { PageText("$it") }).icon = ItemStack(AlfheimBlocks.elvenOre, 1, 4)
		for (i in 0 until (AlfheimBlocks.elvenOre as BlockModMeta).subtypes)
			ores.addExtraDisplayedRecipe(ItemStack(AlfheimBlocks.elvenOre, 1, i))
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.elvenOre, 1, 1), ores, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.elvenOre, 1, 0), ores, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.elvenOre, 1, 2), ores, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.elvenOre, 1, 3), ores, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.elvenOre, 1, 4), ores, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.elvenOre, 1, 5), ores, 2)
		LexiconRecipeMappings.map(IffesalDust.stack, ores, 2)
		
		pastoralSeeds.setLexiconPages(PageText("0"),
									  PageCraftingRecipe("1", AlfheimRecipes.recipesRedstoneRoot),
									  PageManaInfusionRecipe("2", AlfheimRecipes.recipesPastoralSeeds)).setIcon(AlfheimBlocks.rainbowGrass)
		
		for (i in 0..1) {
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowTallGrass, 1, i), pastoralSeeds, 0)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowGrass, 1, i), pastoralSeeds, 0)
		}
		for (i in 0..7) {
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisTallGrass0, 1, i), pastoralSeeds, 0)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisTallGrass1, 1, i), pastoralSeeds, 0)
		}
		for (i in 0..15)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.irisGrass, 1, i), pastoralSeeds, 0)
		for (i in 0..16)
			LexiconRecipeMappings.map(ItemStack(AlfheimItems.irisSeeds, 1, i), pastoralSeeds, 2)
		
		pixie.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipePixieAttractor)).icon = ItemStack(AlfheimItems.pixieAttractor)
		
		portal.setLexiconPages(*Array(3) { PageText("$it") },
							   PageCraftingRecipe("3", AlfheimRecipes.recipeAlfheimPortal),
							   PageText("4"), PageElvenRecipe("5", AlfheimRecipes.recipeInterdimensional),
							   PageMultiblock("6", AlfheimMultiblocks.portal),
							   PageText("7"), PageText("8")).setPriority()
		
		pylons.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeElvenPylon), PageCraftingRecipe("2", ModCraftingRecipes.recipeGaiaPylon)).setPriority().icon = ItemStack(AlfheimBlocks.alfheimPylon, 1, 0)
		
		rainbowFlora.setLexiconPages(PageText("0"),
									 PageCraftingRecipe("1", AlfheimRecipes.recipesRainbowPetal),
									 PageCraftingRecipe("2", AlfheimRecipes.recipeRainbowPetalGrinding),
									 PageCraftingRecipe("3", AlfheimRecipes.recipeRainbowPetalBlock)).icon = ItemStack(AlfheimBlocks.rainbowGrass, 1, 2)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowTallFlower), rainbowFlora, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowGrass, 1, 2), rainbowFlora, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowGrass, 1, 3), LexiconData.shinyFlowers, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowFlowerFloating), LexiconData.shinyFlowers, 3)
		
		reality.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeSword))
		
		redstoneRelay.setLexiconPages(PageText("0"), PageText("1"), PageManaInfusionRecipe("2", AlfheimRecipes.recipeRedstoneRelay))
		
		resonator.setLexiconPages(*Array(5) { PageText("$it") }, PageCraftingRecipe("5", AlfheimRecipes.recipeResonator))
		
		ringsAura.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeAuraRingPink), PageCraftingRecipe("2", AlfheimRecipes.recipeAuraRingElven), PageCraftingRecipe("3", AlfheimRecipes.recipeAuraRingGod)).icon = ItemStack(AlfheimItems.auraRingElven)
		
		ringAnomaly.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeSpatiotemporal))
		
		ringDodge.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeDodgeRing))
		
		ringManaDrive.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRingFeedFlower))
		
		ringSpider.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRingSpider))
		
		rodClick.setLexiconPages(*Array(3) { PageText("$it") }, PageCraftingRecipe("3", AlfheimRecipes.recipeRodClicker))
		
		rodGreen.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRodGreen))
		
		rodPrismatic.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRodPrismatic))
		
		rodRedstone.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeRodRedstone))
		
		rodSuperExchange.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRodSuperExchange))
		
		ruling.setLexiconPages(PageText("0"), PageText("1"),
							   PageCraftingRecipe("2", AlfheimRecipes.recipeRodMuspelheim),
							   PageCraftingRecipe("3", AlfheimRecipes.recipeRodNiflheim),
							   PageText("4"), PageCraftingRecipe("5", listOf(AlfheimRecipes.recipeMuspelheimPendant, AlfheimRecipes.recipeNiflheimPendant))).icon = ItemStack(AlfheimItems.rodMuspelheim)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.redFlame), ruling, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.poisonIce), ruling, 3)
		
		runes.setLexiconPages(PageText("0"), PageRuneRecipe("1", listOf(AlfheimRecipes.recipeMuspelheimRune, AlfheimRecipes.recipeNiflheimRune)),
							  PageText("2"), PageText("3"), PageRuneRecipe("4", AlfheimRecipes.recipeRealityRune)).icon = PrimalRune.stack
		runes.addExtraDisplayedRecipe(NiflheimRune.stack)
		
		sealCreepers.setLexiconPages(PageText("0"), PageText("1${if (AlfheimConfigHandler.blackLotusDropRate > 0.0) "" else "No"}Drop")).setIcon(AlfheimItems.wiltedLotus)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.wiltedLotus, 1, 0), sealCreepers, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.wiltedLotus, 1, 1), sealCreepers, 1)
		
		serenade.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeSerenade))
		
		shimmer.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeShimmerQuartz)).icon = RainbowQuartz.stack
		for (i in arrayOf(0, 1, 2, 5, 6))
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.shimmerQuartz, 1, i), shimmer, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.shimmerQuartzSlab), shimmer, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.shimmerQuartzStairs), shimmer, 0)
		LexiconRecipeMappings.map(RainbowQuartz.stack, shimmer, 1)
		
		shrines.setLexiconPages(PageText("0"), PageText("1")).icon = ItemStack(AlfheimBlocks.powerStone)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.powerStone, 1, 0), shrines, 0)
		for (i in 1..4)
			LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.powerStone, 1, i), shrines, 1)
		
		val slimePages = arrayListOf(
			PageText("0"),
		    PageTuningRecipe("1", AlfheimRecipes.tuningSlimeSize, ItemStack(Items.spawn_egg, 1, 55)),
		    PageTuningRecipe("2", AlfheimRecipes.tuningMagmaSize, ItemStack(Items.spawn_egg, 1, 62)),
		    PageTuningRecipe("3", AlfheimRecipes.tuningElementalSlimeSize, ItemSpawnEgg.forEntity<EntityElementalSlime>()!!)
		)
		
		if (AlfheimRecipes.tuningTaintSize != null) slimePages += PageTuningRecipe("4", AlfheimRecipes.tuningTaintSize!!, ItemStack(ConfigItems.itemSpawnerEgg, 1, 15))
		if (AlfheimRecipes.tuningGelatSize != null) slimePages += PageTuningRecipe("5", AlfheimRecipes.tuningGelatSize!!, ItemStack(TinkerTools.titleIcon))
		
		slimes.setLexiconPages(*slimePages.toTypedArray()).setIcon(Items.slime_ball)
		
		soulSword.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeSoulSword), PageText("3"))
		
		specialAxe.setLexiconPages(PageText("0"), PageText("1")).icon = ItemStack(AlfheimItems.wireAxe)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.wireAxe), specialAxe, 0)
		
		subshroom.setLexiconPages(PageText("0"), PageText("1"))
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.subspacian), subshroom, 0)
		
		temperature.setLexiconPages(PageText("0"), PageText("1")).setPriority()
		
		terraHarvester.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeTerraHarvester))
		
		throwablePotions.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeSplashPotions),
		                                 PageText("2"), PageCraftingRecipe("3", AlfheimRecipes.recipeGrenade)).icon = (AlfheimItems.splashPotion as ItemSplashPotion).getItemForBrew(ModBrews.absorption, null)
		
		toolbelt.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeToolbelt))
		
		trade.setLexiconPages(PageText("0"), PageText("1"),
							  PageCraftingRecipe("2", AlfheimRecipes.recipeElvoriumPylon),
							  PageCraftingRecipe("3", AlfheimRecipes.recipeTradePortal),
							  PageMultiblock("4", AlfheimMultiblocks.yordin)).icon = ItemStack(AlfheimBlocks.tradePortal)
		
		triquetrum.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeTriquetrum))
		
		tunedSaplings.setLexiconPages(PageText("0"), *Array(8) { PageTuningIORecipe("${it+1}", AlfheimRecipes.tuningSaplings[it]) })
		repeat(8) {
			val stack = ItemStack(AlfheimBlocks.tunedSapling, 1, it)
			winery.addExtraDisplayedRecipe(stack)
			LexiconRecipeMappings.map(stack, tunedSaplings, it + 1)
		}
		
		uberSpreader.setLexiconPages(PageText("0"), PageText("1"),
									 if (AlfheimCore.TiCLoaded && !AlfheimCore.stupidMode && AlfheimConfigHandler.materialIDs[TinkersConstructAlfheimConfig.MAUFTRIUM] != -1) PageText("2t")
									 else PageCraftingRecipe(if (AlfheimCore.stupidMode) "2s" else "2", AlfheimRecipes.recipeUberSpreader)).icon = ItemStack(ModBlocks.spreader, 1, 4)
		LexiconRecipeMappings.map(ItemStack(ModBlocks.spreader, 1, 4), uberSpreader, 2)
		
		warBanner.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeWarBanner0))
		
		winery.setLexiconPages(*Array(12) { PageText("$it") },
							   PageCraftingRecipe("12", AlfheimRecipes.recipeBarrel),
							   PageCraftingRecipe("13", AlfheimRecipes.recipeJug))
		winery.addExtraDisplayedRecipe(GrapeLeaf.stack)
		winery.addExtraDisplayedRecipe(Nectar.stack)
		winery.addExtraDisplayedRecipe(RedGrapes.stack)
		winery.addExtraDisplayedRecipe(WhiteGrapes.stack)
		winery.addExtraDisplayedRecipe(RedWine.stack)
		winery.addExtraDisplayedRecipe(WhiteWine.stack)
		LexiconRecipeMappings.map(GrapeLeaf.stack, winery, 0)
		LexiconRecipeMappings.map(RedGrapes.stack, winery, 0)
		LexiconRecipeMappings.map(WhiteGrapes.stack, winery, 0)
		LexiconRecipeMappings.map(RedWine.stack, winery, 0)
		LexiconRecipeMappings.map(WhiteWine.stack, winery, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.grapesRed[0]), winery, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.grapesRed[1]), winery, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.grapesRed[2]), winery, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.grapesRedPlanted), winery, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.grapesWhite), winery, 0)
		
		worldgen.setLexiconPages(PageText("0"),
								 PagePureDaisyRecipe("1", AlfheimRecipes.recipeDreamwood),
			// PageCraftingRecipe("2", AlfheimRecipes.recipeGlowstone),
								 PageText("3"),
								 PageCraftingRecipe("4", AlfheimRecipes.recipeLivingcobble),
								 PageCraftingRecipe("5", AlfheimRecipes.recipeLivingrockPickaxe),
								 PageCraftingRecipe("6", AlfheimRecipes.recipeFurnace),
								 PageCraftingRecipe("7", AlfheimRecipes.recipesApothecary)).icon = ItemStack(AlfheimBlocks.altLeaves, 1, 7)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.elvenSand), worldgen, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.altWood1, 1, 3), worldgen, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.altLeaves, 1, 7), worldgen, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.dreamSapling), worldgen, 0)
		LexiconRecipeMappings.map(DreamCherry.stack, worldgen, 0)
		
		worldTree.setLexiconPages(*Array(2) { PageText("$it") }, PageCraftingRecipe("2", AlfheimRecipes.recipeWorldTree))
		
		(LexiconData.vineBall.pages[4] as PageCraftingRecipe).apply {
			recipes = recipes.toMutableList()
			recipes.add(AlfheimRecipes.recipeLivingCobbleMossy)
		}
		
		LexiconData.decorativeBlocks.setLexiconPages(PageCraftingRecipe("24", AlfheimRecipes.recipesLivingDecor))
		
		(LexiconData.decorativeBlocks.pages[20] as PageCraftingRecipe).apply {
			recipes = recipes.toMutableList()
			recipes.addAll(AlfheimRecipes.recipesRoofTile)
		}
		
		LexiconData.luminizerTransport.setLexiconPages(PageText("7"), PageCraftingRecipe("8", AlfheimRecipes.recipeLuminizer2),
		                                               PageText("9"), PageText("10"), PageCraftingRecipe("11", AlfheimRecipes.recipeLuminizer3))
		
		LexiconData.arcaneRose.pages[0].unlocalizedName += "a"
		
		LexiconData.overgrowthSeed.setLexiconPages(PageText("2"))
		
		if (AlfheimConfigHandler.enableElvenStory) initElvenStory()
		
		// ################################################################
		
		treeCrafting.setLexiconPages(PageText("0"),
									 PageText("1"),
									 PageMultiblock("2", AlfheimMultiblocks.treeCrafter))
			.setPriority().setIcon(AlfheimBlocks.treeCrafterBlockRB)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.treeCrafterBlock), treeCrafting, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.treeCrafterBlockRB), treeCrafting, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.treeCrafterBlockAU), treeCrafting, 2)
		
		lightningSapling.setLexiconPages(PageText("0"),
		                                 PageTreeCrafting("1", AlfheimRecipes.recipeLightningTree),
		                                 PageCraftingRecipe("2", AlfheimRecipes.recipeThunderousPlanks),
		                                 PageCraftingRecipe("3", AlfheimRecipes.recipeThunderousStairs),
		                                 PageCraftingRecipe("4", AlfheimRecipes.recipeThunderousSlabs),
		                                 PageCraftingRecipe("5", AlfheimRecipes.recipeThunderousTwig),
		                                 PageFurnaceRecipe("6", ItemStack(AlfheimBlocks.lightningPlanks)))
				.setIcon(AlfheimBlocks.lightningSapling)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.lightningSapling), lightningSapling, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.lightningWood), lightningSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.lightningLeaves), lightningSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.lightningPlanks), lightningSapling, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.lightningSlabs), lightningSapling, 3)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.lightningStairs), lightningSapling, 4)
		LexiconRecipeMappings.map(ThunderwoodTwig.stack, lightningSapling, 5)
		LexiconRecipeMappings.map(ThunderwoodSplinters.stack, lightningSapling, 6)
		
		netherSapling.setLexiconPages(PageText("0"),
		                              PageTreeCrafting("1", AlfheimRecipes.recipeInfernalTree),
		                              PageCraftingRecipe("2", AlfheimRecipes.recipeInfernalPlanks),
		                              PageCraftingRecipe("3", AlfheimRecipes.recipeInfernalStairs),
		                              PageCraftingRecipe("4", AlfheimRecipes.recipeInfernalSlabs),
		                              PageCraftingRecipe("5", AlfheimRecipes.recipeInfernalTwig),
		                              PageFurnaceRecipe("6", ItemStack(AlfheimBlocks.netherWood)),
		                              PageFurnaceRecipe("7", ItemStack(AlfheimBlocks.netherPlanks)))
				.setIcon(AlfheimBlocks.netherSapling)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.netherSapling), netherSapling, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.netherWood), netherSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.netherLeaves), netherSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.netherPlanks), netherSapling, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.netherSlabs), netherSapling, 3)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.netherStairs), netherSapling, 4)
		LexiconRecipeMappings.map(NetherwoodTwig.stack, netherSapling, 5)
		LexiconRecipeMappings.map(NetherwoodSplinters.stack, netherSapling, 6)
		LexiconRecipeMappings.map(NetherwoodCoal.stack, netherSapling, 7)
		
		circuitSapling.setLexiconPages(PageText("0"),
		                               PageTreeCrafting("1", AlfheimRecipes.recipeCircuitTree),
		                               PageCraftingRecipe("2", AlfheimRecipes.recipeCircuitPlanks),
		                               PageCraftingRecipe("3", AlfheimRecipes.recipeCircuitStairs),
		                               PageCraftingRecipe("4", AlfheimRecipes.recipeCircuitSlabs))
				.setIcon(AlfheimBlocks.circuitSapling)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.circuitWood), circuitSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.circuitLeaves), circuitSapling, 0)
		
		calicoSapling.setLexiconPages(PageText("0"),
		                              PageTreeCrafting("1", AlfheimRecipes.recipeCalicoTree),
		                              PageCraftingRecipe("2", AlfheimRecipes.recipeCalicoPlanks),
		                              PageCraftingRecipe("3", AlfheimRecipes.recipeCalicoStairs),
		                              PageCraftingRecipe("4", AlfheimRecipes.recipeCalicoSlabs))
				.setIcon(AlfheimBlocks.calicoSapling)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.calicoWood), calicoSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.calicoLeaves), calicoSapling, 0)
		
		silencer.setLexiconPages(PageText("0"),
		                         PageTreeCrafting("1", AlfheimRecipes.recipeSealingTree),
		                         PageCraftingRecipe("2", AlfheimRecipes.recipeSealingPlanks),
		                         PageCraftingRecipe("3", AlfheimRecipes.recipeSealingStairs),
		                         PageCraftingRecipe("4", AlfheimRecipes.recipeSealingSlabs))
				.setIcon(AlfheimBlocks.sealingSapling)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.sealingSapling), silencer, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.sealingWood), silencer, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.sealingLeaves), silencer, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.sealingPlanks), silencer, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.sealingStairs), silencer, 3)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.sealingSlabs), silencer, 4)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.amplifier), amplifier, 1)
		
		barrierSapling.setLexiconPages(PageText("0"),
									   PageTreeCrafting("1", AlfheimRecipes.recipeBarrierTree),
									   PageCraftingRecipe("2", AlfheimRecipes.recipeBarrierPlanks),
									   PageCraftingRecipe("3", AlfheimRecipes.recipeBarrierStairs),
									   PageCraftingRecipe("4", AlfheimRecipes.recipeBarrierSlabs))
					  .setIcon(AlfheimBlocks.barrierSapling)
		
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.barrierSapling), barrierSapling, 1)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.barrierWood), barrierSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.barrierLeaves), barrierSapling, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.barrierPlanks), barrierSapling, 2)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.barrierStairs), barrierSapling, 3)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.barrierSlabs), barrierSapling, 4)
		
		treeBerry.setLexiconPages(*Array(7) { PageText("$it") })
		LexiconRecipeMappings.map(TreeBerryLightning.stack, treeBerry, 1)
		LexiconRecipeMappings.map(TreeBerryNether.stack, treeBerry, 2)
		LexiconRecipeMappings.map(TreeBerryCircuit.stack, treeBerry, 3)
		LexiconRecipeMappings.map(TreeBerryCalico.stack, treeBerry, 4)
		LexiconRecipeMappings.map(TreeBerrySealing.stack, treeBerry, 5)
		LexiconRecipeMappings.map(TreeBerryBarrier.stack, treeBerry, 6)
		
		HV.setLexiconPages(*Array(5) { PageText("$it") })
			.setKnowledgeType(elvenKnowledge)
			.icon = ItemStack(AlfheimItems.eventResource, 1, EventResourcesMetas.VolcanoRelic)
		
		WOTW.setLexiconPages(*Array(3) { PageText("$it") })
			.setKnowledgeType(elvenKnowledge)
			.icon = ItemStack(AlfheimItems.eventResource, 1, EventResourcesMetas.SnowRelic)
		
		if (ThaumcraftSuffusionRecipes.recipesLoaded) {
			tctrees = object: AlfheimLexiconEntry("tctrees", categoryDendrology) {
				override fun getSubtitle() = "[Alfheim x Thaumcraft]"
			}
			
			tctrees.setLexiconPages(PageText("0"),
			                        PageTreeCrafting("1", ThaumcraftSuffusionRecipes.greatwoodRecipe),
			                        PageTreeCrafting("2", ThaumcraftSuffusionRecipes.silverwoodRecipe),
			                        PageText("3"),
			                        PageTreeCrafting("4", ThaumcraftSuffusionRecipes.shimmerleafRecipe),
			                        PageTreeCrafting("5", ThaumcraftSuffusionRecipes.cinderpearlRecipe),
			                        PageTreeCrafting("6", ThaumcraftSuffusionRecipes.vishroomRecipe)).icon = ItemStack(ThaumcraftSuffusionRecipes.plantBlock)
			
			LexiconRecipeMappings.map(ItemStack(ThaumcraftSuffusionRecipes.plantBlock, 1, 0), tctrees, 1)
			LexiconRecipeMappings.map(ItemStack(ThaumcraftSuffusionRecipes.plantBlock, 1, 1), tctrees, 2)
			LexiconRecipeMappings.map(ItemStack(ThaumcraftSuffusionRecipes.plantBlock, 1, 2), tctrees, 4)
			LexiconRecipeMappings.map(ItemStack(ThaumcraftSuffusionRecipes.plantBlock, 1, 3), tctrees, 5)
			LexiconRecipeMappings.map(ItemStack(ThaumcraftSuffusionRecipes.plantBlock, 1, 5), tctrees, 6)
			
			LexiconRecipeMappings.map(ItemStack(ModItems.elementiumHelmRevealing), LexiconData.tcIntegration, 2)
			LexiconRecipeMappings.map(ItemStack(ModItems.terrasteelHelmRevealing), LexiconData.tcIntegration, 2)
			LexiconRecipeMappings.map(ItemStack(AlfheimItems.elementalHelmetRevealing), LexiconData.tcIntegration, 2)
			LexiconRecipeMappings.map(ItemStack(AlfheimItems.elvoriumHelmetRevealing), LexiconData.tcIntegration, 2)
			LexiconRecipeMappings.map(ItemStack(AlfheimItems.fenrirHelmetRevealing), LexiconData.tcIntegration, 2)
			LexiconRecipeMappings.map(ItemStack(AlfheimItems.snowHelmetRevealing), LexiconData.tcIntegration, 2)
			LexiconRecipeMappings.map(ItemStack(AlfheimItems.volcanoHelmetRevealing), LexiconData.tcIntegration, 2)
		}
		
		// ################################################################
		
		divIntro.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeAttribution), PageText("3")).setPriority()
		abyss.setLexiconPages(PageText("0"), PageText("1"),
		                      PageTextConditional("2") { ASJUtilities.isServer || "${Knowledge.ABYSS_TRUTH}" in PlayerSegmentClient.knowledge },
		                      PageTextConditional("3") { ASJUtilities.isServer || "${Knowledge.NIFLHEIM}" in PlayerSegmentClient.knowledge },
		                      PageTextConditional("4") { ASJUtilities.isServer || "${Knowledge.NIFLHEIM_POST}" in PlayerSegmentClient.knowledge },
		                      PageTextConditional("5") { ASJUtilities.isServer || "${Knowledge.MUSPELHEIM}" in PlayerSegmentClient.knowledge },
		                      PageTextConditional("6") { ASJUtilities.isServer || "${Knowledge.MUSPELHEIM_POST}" in PlayerSegmentClient.knowledge }).setPriority()
		
		vafthrudnir.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeRealityAnchor), PageMultiblock("3", AlfheimMultiblocks.anchor),
		                            *Array(4) { PageText("${it+4}") },
			                        PageText("8"), PageText("9"), PageManaInfusionRecipe("10", AlfheimRecipes.recipeRiftShard),
		                            PageCraftingRecipe("11", AlfheimRecipes.recipesRealmCore),
			                        PageCraftingRecipe("12", AlfheimRecipes.recipesRealmFrame),
			                        PageCraftingRecipe("13", AlfheimRecipes.recipeSpire),
			                        PageCraftingRecipe("14", AlfheimRecipes.recipeCreationPylon),
			                        PageMultiblock("15", AlfheimMultiblocks.spire),
			                        PageText("16"), PageText("17")).setPriority().setIcon(AlfheimBlocks.spire)
		
		LexiconRecipeMappings.remove(AlfheimRecipes.recipeRealityAnchor.recipeOutput)
		LexiconRecipeMappings.remove(AlfheimRecipes.recipeRiftShard.output)
		LexiconRecipeMappings.remove(AlfheimRecipes.recipesRealmCore[0].recipeOutput)
		LexiconRecipeMappings.remove(AlfheimRecipes.recipesRealmCore[1].recipeOutput)
		LexiconRecipeMappings.remove(AlfheimRecipes.recipesRealmFrame[0].recipeOutput)
		LexiconRecipeMappings.remove(AlfheimRecipes.recipesRealmFrame[1].recipeOutput)
		LexiconRecipeMappings.remove(AlfheimRecipes.recipeSpire.recipeOutput)
		LexiconRecipeMappings.remove(AlfheimRecipes.recipeCreationPylon.recipeOutput)
		LexiconRecipeMappings.remove(AlfheimRecipes.recipeRealityAnchor.recipeOutput)
		
		cloakThor.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCloakThor))
		cloakSif.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCloakSif))
		cloakNjord.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCloakNjord))
		cloakLoki.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCloakLoki))
		cloakHeimdall.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCloakHeimdall))
		cloakOdin.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeCloakOdin))
		
		emblemThor.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipePriestOfThor))
		emblemSif.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipePriestOfSif))
		emblemNjord.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipePriestOfNjord))
		emblemLoki.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipePriestOfLoki))
		emblemHeimdall.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipePriestOfHeimdall))
		emblemOdin.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipePriestOfOdin))
		
		rodThor.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRodLightning), PageText("2"))
		rodSif.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipesRodColoredSkyDirt), PageText("2")).icon = ItemStack(AlfheimItems.rodColorfulSkyDirt, 1, 16)
		rodNjord.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRodInterdiction), PageText("2"))
		rodLoki.setLexiconPages(PageText("0"), PageCraftingRecipe("1", AlfheimRecipes.recipeRodFlame), PageText("2"))
		rodOdin.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeRodPortal), PageText("3"))
		
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.rodColorfulSkyDirt), rodSif, 1)

//		var memes = LexiconData.tinyPotato
//		for (entry in BotaniaAPI.getAllEntries())
//			if (entry.getUnlocalizedName() == "botania.entry.wrap")
//				memes = entry
//
//		LexiconRecipeMappings.map(ItemStack(AlfheimItems.attributionBauble, 1, 1), memes, 1)
		
		LexiconData.thorRing.apply {
			pages[0].unlocalizedName += "n"
			category.entries.remove(this)
			category = categoryDivinity
			categoryDivinity.entries.add(this)
		}
		
		LexiconData.lokiRing.apply {
			pages[0].unlocalizedName += "n"
			pages[3].unlocalizedName += "n"
			
			category.entries.remove(this)
			category = categoryDivinity
			categoryDivinity.entries.add(this)
		}
		
		LexiconData.odinRing.apply {
			pages[0].unlocalizedName += "n"
			category.entries.remove(this)
			category = categoryDivinity
			categoryDivinity.entries.add(this)
		}
		
		LexiconData.infiniteFruit.apply {
			category.entries.remove(this)
			category = categoryDivinity
			categoryDivinity.entries.add(this)
		}
		
		LexiconData.rainbowRod.apply {
			category.entries.remove(this)
			category = categoryDivinity
			categoryDivinity.entries.add(this)
		}
		
		// ################################################################
		// ################################################################
		// ################################################################
		
		LexiconData.cosmeticBaubles.setLexiconPages(PageCraftingRecipe("34", AlfheimRecipes.recipeThinkingHand))
		
		LexiconData.pool.setLexiconPages(PageManaInfusionRecipe("15", AlfheimRecipes.recipeInfusedDreamTwig))
		
		LexiconData.lenses.setLexiconPages(
			PageText("38"), PageCraftingRecipe("39", AlfheimRecipes.recipeLensMessenger),
			PageText("40"), PageCraftingRecipe("41", AlfheimRecipes.recipeLensPush),
			PageText("42"), PageCraftingRecipe("43", AlfheimRecipes.recipeLensSmelt),
			PageText("44"), PageCraftingRecipe("45", AlfheimRecipes.recipeLensTrack),
		)
		
		LexiconData.elvenLenses.setLexiconPages(
			PageText("11"), PageCraftingRecipe("12", AlfheimRecipes.recipeLensTripwire),
			PageText("13"), PageCraftingRecipe("14", AlfheimRecipes.recipeLensSuperconductor),
			PageText("15"), PageCraftingRecipe("16", AlfheimRecipes.recipeLensPurification),
			PageText("17"), PageCraftingRecipe("18", AlfheimRecipes.recipeLensLinkback),
			PageText("19"), PageCraftingRecipe("20", AlfheimRecipes.recipeLensUnlink),
		)
		
		PageText("botania.page.judgementCloaks1n").apply { LexiconData.judgementCloaks.pages[1] = this }.onPageAdded(LexiconData.judgementCloaks, 1)
		LexiconData.judgementCloaks.setLexiconPages(PageCraftingRecipe("4", AlfheimRecipes.recipeBalanceCloak))
		
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.invisibleFlameLens), LexiconData.lenses, 35)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.rainbowMushroom), LexiconData.mushrooms, 1)
		
		setKnowledgeTypes()
		replaceBotaniaRecipes()
	}
	
	fun initRelics() {
		akashic = AlfheimRelicLexiconEntry("akashic", pickCategory(categoryAlfhomancy))
		akashic.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningAkashicRecords))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.akashicRecords), akashic, 0)
		
		daolos = AlfheimRelicLexiconEntry("daolos", categoryDivinity, AlfheimItems.daolos)
		daolos.setLexiconPages(*Array(6) { PageText("$it") }, PageTuningIORecipe("6", AlfheimRecipes.tuningDaolos))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.daolos), daolos, 0)
		
		excaliber = AlfheimRelicLexiconEntry("excaliber", pickCategory(categoryAlfhomancy), AlfheimItems.excaliber)
		excaliber.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningExcaliber))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.excaliber), excaliber, 0)
		
		gjallarhorn = AlfheimRelicLexiconEntry("gjallarhorn", categoryDivinity, AlfheimItems.gjallarhorn)
		gjallarhorn.setLexiconPages(PageText("0"), PageText("1"), PageTuningIORecipe("2", AlfheimRecipes.tuningGjallarhorn))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.gjallarhorn), gjallarhorn, 0)
		
		gleipnir = AlfheimRelicLexiconEntry("gleipnir", categoryDivinity, AlfheimItems.gleipnir)
		gleipnir.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningGleipnir))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.gleipnir), gleipnir, 0)
		
		gungnir = AlfheimRelicLexiconEntry("gungnir", categoryDivinity, AlfheimItems.gungnir)
		gungnir.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningGungnir))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.gungnir), gungnir, 0)
		
		mask = AlfheimRelicLexiconEntry("mask", pickCategory(categoryAlfhomancy), AlfheimItems.mask)
		mask.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningTankMask))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.mask), mask, 0)
		
		mjolnir = AlfheimRelicLexiconEntry("mjolnir", categoryDivinity, AlfheimItems.mjolnir)
		mjolnir.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningMjolnir))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.mjolnir), mjolnir, 0)
		
		moonbow = AlfheimRelicLexiconEntry("moonbow", pickCategory(categoryAlfhomancy), AlfheimItems.moonlightBow)
		moonbow.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningMoonlightBow))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.moonlightBow), moonbow, 0)
		
		ringHeimdall = AlfheimRelicLexiconEntry("ring_heimdall", categoryDivinity, AlfheimItems.priestRingHeimdall)
		ringHeimdall.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningHeimdallRing))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.priestRingHeimdall), ringHeimdall, 0)
		
		ringNjord = AlfheimRelicLexiconEntry("ring_njord", categoryDivinity, AlfheimItems.priestRingNjord)
		ringNjord.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningNjordRing))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.priestRingNjord), ringNjord, 0)
		
		ringSif = AlfheimRelicLexiconEntry("ring_sif", categoryDivinity, AlfheimItems.priestRingSif)
		ringSif.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningSifRing))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.priestRingSif), ringSif, 0)
		
		soul = AlfheimRelicLexiconEntry("soul", pickCategory(categoryAlfhomancy), AlfheimItems.flugelSoul)
		soul.setLexiconPages(*Array(9) { PageText("$it") },
							 PageMultiblock("9", AlfheimMultiblocks.soul),
							 PageText("10"), PageCraftingRecipe("11", AlfheimRecipes.recipeCleanPylon),
							 PageTuningIORecipe("12", AlfheimRecipes.tuningFlugelSoul))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.flugelSoul), soul, 0)
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.alfheimPylon, 1, 2), soul, 7)
		
		soulHorn = AlfheimRelicLexiconEntry("soulHorn", pickCategory(categoryAlfhomancy), AlfheimAchievements.flugelHardKill)
		soulHorn.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipeSoulHorn), PageText("3")).icon = ItemStack(AlfheimItems.soulHorn)
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.soulHorn, 1, 1), soulHorn, 2)
		
		subspear = AlfheimRelicLexiconEntry("subspear", pickCategory(categoryAlfhomancy), AlfheimItems.subspaceSpear)
		subspear.setLexiconPages(PageText("0"), PageTuningIORecipe("1", AlfheimRecipes.tuningSpearSubspace))
		LexiconRecipeMappings.map(ItemStack(AlfheimItems.subspaceSpear), subspear, 0)
		
		if (ConfigHandler.relicsEnabled) {
			LexiconData.flugelEye.setLexiconPages(PageTuningIORecipe("2", AlfheimRecipes.tuningFlugelEye))
			LexiconData.infiniteFruit.setLexiconPages(PageTuningIORecipe("1", AlfheimRecipes.tuningInfiniteFruit))
			LexiconData.kingKey.setLexiconPages(PageTuningIORecipe("1", AlfheimRecipes.tuningKingKey))
			LexiconData.lokiRing.setLexiconPages(PageTuningIORecipe("4", AlfheimRecipes.tuningLokiRing))
			LexiconData.odinRing.setLexiconPages(PageTuningIORecipe("1", AlfheimRecipes.tuningOdinRing))
			LexiconData.thorRing.setLexiconPages(PageTuningIORecipe("1", AlfheimRecipes.tuningThorRing))
			
			LexiconRecipeMappings.map(ItemStack(ModItems.dice), LexiconData.relicInfo, 0)
			LexiconRecipeMappings.map(ItemStack(ModItems.infiniteFruit), LexiconData.infiniteFruit, 0)
			LexiconRecipeMappings.map(ItemStack(ModItems.kingKey), LexiconData.kingKey, 0)
			LexiconRecipeMappings.map(ItemStack(ModItems.flugelEye), LexiconData.flugelEye, 0)
			LexiconRecipeMappings.map(ItemStack(ModItems.thorRing), LexiconData.thorRing, 0)
			LexiconRecipeMappings.map(ItemStack(ModItems.lokiRing), LexiconData.lokiRing, 0)
			LexiconRecipeMappings.map(ItemStack(ModItems.odinRing), LexiconData.odinRing, 0)
		}
	}
	
	private fun initElvenStory() {
		if (esm!!.pages.isEmpty())
			esm!!.setPriority()
				.setLexiconPages(PageText("0"))
		
		if (races!!.pages.isEmpty())
			races!!.setPriority().setLexiconPages(*Array(6) { PageText("$it") })
		LexiconRecipeMappings.map(ItemStack(AlfheimBlocks.raceSelector), races, 0)
		races?.icon = null
		
		if (AlfheimConfigHandler.enableMMO) initMMO()
	}
	
	private fun initMMO() {
		if (parties!!.pages.isEmpty())
			parties!!.setPriority()
				.setLexiconPages(PageText("0"), PageText("1"), PageCraftingRecipe("2", AlfheimRecipes.recipePeacePipe),
								 PageText("3"), PageCraftingRecipe("4", AlfheimRecipes.recipePaperBreak)).icon = null
		
		if (spells!!.pages.isEmpty()) {
			spells!!.setPriority()
				.setLexiconPages(*Array(4) { PageText("$it") })
			
			val l = ArrayList(AlfheimAPI.spells)
			l.sortBy { it.name }
			for (spell in l) spells!!.addPage(PageSpell(spell))
		}
		
		if (targets!!.pages.isEmpty())
			targets!!.setPriority()
				.setLexiconPages(PageText("0"), PageText("1"))
	}
	
	private fun replaceBotaniaRecipes() {
		LexiconData.superLavaPendant.pages.filterIsInstance<PageCraftingRecipe>().first().recipes = listOf(ModCraftingRecipes.recipeSuperLavaPendant)
		LexiconData.gaiaRitual.pages.filterIsInstance<PageCraftingRecipe>().first().recipes = listOf(ModCraftingRecipes.recipeGaiaPylon)
		LexiconData.rainbowRod.pages.filterIsInstance<PageCraftingRecipe>().first { "4" in it.unlocalizedName }.recipes = listOf(ModCraftingRecipes.recipeShimmerrock)
		LexiconData.rainbowRod.pages.filterIsInstance<PageCraftingRecipe>().first { "5" in it.unlocalizedName }.recipes = listOf(ModCraftingRecipes.recipeShimmerwoodPlanks)
		LexiconData.dreamwoodSpreader.pages.filterIsInstance<PageCraftingRecipe>().first { "3" in it.unlocalizedName }.recipes = listOf(ModCraftingRecipes.recipeUltraSpreader)
	}
	
	private fun setKnowledgeTypes() {
		advancedMana.knowledgeType = elvenKnowledge
		alfheim.knowledgeType = elvenKnowledge
		amplifier.knowledgeType = elvenKnowledge
		amuletIceberg.knowledgeType = elvenKnowledge
		amuletNimbus.knowledgeType = elvenKnowledge
		amulterCrescent.knowledgeType = elvenKnowledge
		anomaly.knowledgeType = elvenKnowledge
		anyavil.knowledgeType = elvenKnowledge
		armilla.knowledgeType = elvenKnowledge
		astrolabe.knowledgeType = elvenKnowledge
		beltRation.knowledgeType = elvenKnowledge
		chalk.knowledgeType = elvenKnowledge
		corpInj.knowledgeType = elvenKnowledge
		corpQuandex.knowledgeType = elvenKnowledge
		corpSeq.knowledgeType = elvenKnowledge
		colorOverride.knowledgeType = elvenKnowledge
		dasGold.knowledgeType = elvenKnowledge
		deathSeed.knowledgeType = elvenKnowledge
		elementalSet.knowledgeType = elvenKnowledge
		elvenSet.knowledgeType = elvenKnowledge
		elves.knowledgeType = elvenKnowledge
		elvorium.knowledgeType = elvenKnowledge
		essences.knowledgeType = elvenKnowledge
		fenrir.knowledgeType = elvenKnowledge
		fenrirCloak.knowledgeType = elvenKnowledge
		fenrirDrop.knowledgeType = elvenKnowledge
		fenrirGlove.knowledgeType = elvenKnowledge
		flowerAlfchid.knowledgeType = elvenKnowledge
		flowerBud.knowledgeType = elvenKnowledge
		flowerEnderchid.knowledgeType = elvenKnowledge
		flowerPetronia.knowledgeType = elvenKnowledge
		flugel.knowledgeType = elvenKnowledge
		fracturedSpace.knowledgeType = elvenKnowledge
		gaiaButton.knowledgeType = elvenKnowledge
		hyperBucket.knowledgeType = elvenKnowledge
		infuser.knowledgeType = elvenKnowledge
		ivySave.knowledgeType = elvenKnowledge
		lamp.knowledgeType = elvenKnowledge
		legends.knowledgeType = elvenKnowledge
		lembas.knowledgeType = elvenKnowledge
		lootInt.knowledgeType = elvenKnowledge
		manaImba.knowledgeType = elvenKnowledge
		manaLamp.knowledgeType = elvenKnowledge
		mobs.knowledgeType = elvenKnowledge
		multbauble.knowledgeType = elvenKnowledge
		ores.knowledgeType = elvenKnowledge
		openChest.knowledgeType = elvenKnowledge
		pixie.knowledgeType = elvenKnowledge
		portal.knowledgeType = elvenKnowledge
		pylons.knowledgeType = elvenKnowledge
		rainbowFlora.knowledgeType = elvenKnowledge
		reality.knowledgeType = elvenKnowledge
		ringAnomaly.knowledgeType = elvenKnowledge
		ringsAura.knowledgeType = elvenKnowledge
		rodClick.knowledgeType = elvenKnowledge
		rodPrismatic.knowledgeType = elvenKnowledge
		rodRedstone.knowledgeType = elvenKnowledge
		rodSuperExchange.knowledgeType = elvenKnowledge
		ruling.knowledgeType = elvenKnowledge
		runes.knowledgeType = elvenKnowledge
		serenade.knowledgeType = elvenKnowledge
		shimmer.knowledgeType = elvenKnowledge
		shrines.knowledgeType = elvenKnowledge
		silencer.knowledgeType = elvenKnowledge
		soulSword.knowledgeType = elvenKnowledge
		subshroom.knowledgeType = elvenKnowledge
		chakramThunder.knowledgeType = elvenKnowledge
		trade.knowledgeType = elvenKnowledge
		uberSpreader.knowledgeType = elvenKnowledge
		winery.knowledgeType = elvenKnowledge
		worldgen.knowledgeType = elvenKnowledge
		worldTree.knowledgeType = elvenKnowledge
		
		abyss.knowledgeType = elvenKnowledge
		vafthrudnir.knowledgeType = elvenKnowledge
		
		emblemThor.knowledgeType = elvenKnowledge
		emblemSif.knowledgeType = elvenKnowledge
		emblemNjord.knowledgeType = elvenKnowledge
		emblemLoki.knowledgeType = elvenKnowledge
		emblemHeimdall.knowledgeType = elvenKnowledge
		emblemOdin.knowledgeType = elvenKnowledge
		
		cloakThor.knowledgeType = elvenKnowledge
		cloakSif.knowledgeType = elvenKnowledge
		cloakNjord.knowledgeType = elvenKnowledge
		cloakLoki.knowledgeType = elvenKnowledge
		cloakHeimdall.knowledgeType = elvenKnowledge
		cloakOdin.knowledgeType = elvenKnowledge
		
		rodThor.knowledgeType = elvenKnowledge
		rodSif.knowledgeType = elvenKnowledge
		rodNjord.knowledgeType = elvenKnowledge
		rodLoki.knowledgeType = elvenKnowledge
		rodOdin.knowledgeType = elvenKnowledge
		
		if (ThaumcraftSuffusionRecipes.recipesLoaded) {
			tctrees.knowledgeType = elvenKnowledge
		}
	}
	
	fun disableESM() {
		setKnowledgeTypes()
		
		removeEntry(esm, categoryAlfheim)
		removeEntry(races, categoryAlfheim)
	}
	
	fun reEnableESM() {
		if (AlfheimConfigHandler.enableElvenStory) {
			preInitElvenStory()
			initElvenStory()
		}
		if (AlfheimConfigHandler.enableMMO) {
			preInitMMO()
			initMMO()
		}
		
		if (!categoryAlfheim.entries.contains(esm)) addEntry(esm, categoryAlfheim)
		if (!categoryAlfheim.entries.contains(races)) addEntry(races, categoryAlfheim)
		
		setKnowledgeTypes()
	}
	
	fun disableMMO() {
		setKnowledgeTypes()
		
		removeEntry(parties, categoryAlfheim)
		removeEntry(spells, categoryAlfheim)
		removeEntry(targets, categoryAlfheim)
	}
	
	fun reEnableMMO() {
		if (AlfheimConfigHandler.enableElvenStory) {
			preInitElvenStory()
			initElvenStory()
		}
		if (AlfheimConfigHandler.enableMMO) {
			preInitMMO()
			initMMO()
		}
		
		if (!categoryAlfheim.entries.contains(parties)) addEntry(parties, categoryAlfheim)
		if (!categoryAlfheim.entries.contains(spells)) addEntry(spells, categoryAlfheim)
		if (!categoryAlfheim.entries.contains(targets)) addEntry(targets, categoryAlfheim)
		
		setKnowledgeTypes()
	}
	
	private fun removeEntry(entry: LexiconEntry?, category: LexiconCategory) {
		getAllEntries().remove(entry)
		category.entries.remove(entry)
	}
	
	private fun pickCategory(alt: LexiconCategory) = if (AlfheimConfigHandler.lexiconSort) alt else categoryAlfheim
}

object AlfheimMultiblocks {
	val anchor = TileRealityAnchor.makeMultiblockSet()
	val infuser = TileManaInfuser.makeMultiblockSet()
	val infuserU = TileManaInfuser.makeMultiblockSetUnknown()
	val portal = TileAlfheimPortal.makeMultiblockSet()
	val soul = TileManaInfuser.makeMultiblockSetSoul()
	val spire = TileSpire.makeMultiblockSet()
	val treeCrafter = TileTreeCrafter.makeMultiblockSet()
	val yordin = TileTradePortal.makeMultiblockSet()
}
